(function() {
    'use strict';

    angular
        .module('app.employeebenefit')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'employeebenefit',
                config: {
                    url: '/employeebenefit',
                    templateUrl: 'app/admin/employee-benefit/employeebenefit.html',
                    controller: 'EmployeebenefitController',
                    controllerAs: 'vm',
                    title: 'employeebenefit',
                    settings: {
                        nav: 2,
                        parent:'admin',
                        // content: '<i class="employee-benefit-logo"></i> <span>Employee Benefit</span>'
                        name: "EMPLOYEE_BENEFIT",
                        className: "fa fa-diamond"
                    }
                }
            }
        ];
    }
})();
