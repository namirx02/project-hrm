	(function () {
    'use strict';

    angular
        .module('app.employeebenefit', ['ngMaterial'])
        .controller('EmployeebenefitController', EmployeebenefitController);

        EmployeebenefitController.$inject = ['$q', 'countryservice', 'adminservice', 'dataservice', 'profileservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function EmployeebenefitController($q, countryservice, adminservice, dataservice, profileservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'Employee Benefit';
        vm.queryTags = queryTags;

        activate();

        function activate() {
            var promises = [init()];
            return $q.all(promises).then(function() {
            });
        }

        function queryTags(query) {
            return profileservice.getProfilesByString({page: 1, string: query}).then(function(data) {
                var filteredNames = data === false ? [] : data;

                return $q.when(filteredNames);
            })
        }

        function init() {
            vm.countries = countryservice.countries();
            setTimeout(
              function loadScript() {
                  $(function() {
                      $('#benefit-date').datetimepicker({
                          format: 'DD/MM/YYYY'
                      });
                  });
              }
            );
        }
    }
})();
