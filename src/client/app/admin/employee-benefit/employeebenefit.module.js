(function() {
    'use strict';

    angular.module('app.employeebenefit', [
        'app.core',
        'app.widgets'
      ]);
})();
