/* jshint -W117, -W030 */
describe('employeebenefit routes', function () {
    describe('state', function () {
        var view = 'app/employee-benefit/employeebenefit.html';

        beforeEach(function() {
            module('app.employeebenefit', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state admin to url / ', function() {
            expect($state.href('employeebenefit', {})).to.equal('/');
        });

        it('should map /admin route to admin View template', function () {
            expect($state.get('employeebenefit').templateUrl).to.equal(view);
        });

        it('of admin should work with $state.go', function () {
            $state.go('employeebenefit');
            $rootScope.$apply();
            expect($state.is('employeebenefit'));
        });
    });
});
