(function() {
    'use strict';

    angular.module('app.createaccount', [
        'app.core',
        'app.widgets'
      ]);
})();
