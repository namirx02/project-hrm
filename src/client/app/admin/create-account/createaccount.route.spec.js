/* jshint -W117, -W030 */
describe('createaccount routes', function () {
    describe('state', function () {
        var view = 'app/create-account/createaccount.html';

        beforeEach(function() {
            module('app.createaccount', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state admin to url / ', function() {
            expect($state.href('createaccount', {})).to.equal('/');
        });

        it('should map /admin route to admin View template', function () {
            expect($state.get('createaccount').templateUrl).to.equal(view);
        });

        it('of admin should work with $state.go', function () {
            $state.go('createaccount');
            $rootScope.$apply();
            expect($state.is('createaccount'));
        });
    });
});
