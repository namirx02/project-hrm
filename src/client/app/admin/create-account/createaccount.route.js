(function() {
    'use strict';

    angular
        .module('app.createaccount')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'createaccount',
                config: {
                    url: '/create-account',
                    templateUrl: 'app/admin/create-account/createaccount.html',
                    controller: 'CreateaccountController',
                    controllerAs: 'vm',
                    title: 'createaccount',
                    settings: {
                        nav: 2,
                        parent:'admin',
                        // content: '<i class="create-user-logo"></i> <span>Create Account</span>'
                        name: "ACCOUNT_MANAGEMENT",
                        className: "create-user-logo"
                    }
                }
            }
        ];
    }
})();
