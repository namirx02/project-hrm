	(function () {
    'use strict';

    angular
        .module('app.createaccount')
        .controller('CreateaccountController', AdminController);

    AdminController.$inject = ['$q', 'countryservice', 'profileservice', 'adminservice', 'dataservice', 'officeservice', 'logger'];
    /* @ngInject */
    function AdminController($q, countryservice, profileservice, adminservice, dataservice, officeservice, logger) {
        var vm = this;

        vm.title = 'Create Account';

        // Variables
        vm.departments = [];
        vm.availableDepts = [];
        vm.offices = [];
		vm.viewInfo = 1;

		vm.countries = [];
        vm.query = {
            keyword: "",
            letter: "",
            office: -1,
            department: -1,
            page: 1
        };
        vm.currentPage = 1;
        vm.LastPage = 1;
        vm.paginationList = [];
        // New pagination
        vm.getNextProfilePageByQuery = getNextProfilePageByQuery;
        vm.getPreviousProfilePageByQuery = getPreviousProfilePageByQuery;
        vm.getPageByQuery = getPageByQuery;

        // functions
        vm.createProfile = createProfile;
		vm.toggleView = toggleView;
        vm.updateDepartmentSource = updateDepartmentSource;

        activate();

        function activate() {
            var promises = [init(), getOffices(), getDepartments(), getProfilesByQuery(vm.query)];

            return $q.all(promises).then(function() {
                logger.info('Activated Create Account View');
            });
        }

        function init() {
            vm.countries = countryservice.countries();
            setTimeout(
              function loadScript() {
                  $(function() {
                      $('#BirthDate').datetimepicker({
                          format: 'DD/MM/YYYY',
                          viewMode: 'years'
                      });

                      $('#StartDate').datetimepicker({
                          format: 'DD/MM/YYYY'
                      });

                  });
              }
            );
        }

        function getOffices(){
            return dataservice.getOffices().then(function (data){
                vm.offices = data;
            });
        }

        function getDepartments(){
            return dataservice.getDepartments().then(function (data){
                vm.departments = data;
            });
        }

        function updateDepartmentSource(office_id){
            vm.availableDepts = vm.departments.filter(function(item) {
               return item.office.id == office_id;
            });
        }

        function createProfile(profile, address, domicile, department_id, office_id, payroll, verifyEmail) {

            var isEmptyField = false;
            var errorMessage = '';


            if(profile && address && domicile){     //check if profile object exists or address object exists or not (It does not exist if all of the fields of an object is empty)

                // check for empty field
                // --------- Personal Details ---------
                if(!profile.name){
                    errorMessage += '\"Last name\" ';
                    isEmptyField = true;
                }

                if(!profile.email){
                    errorMessage += '\"Email\" ';
                    isEmptyField = true;
                }

                if(!$('#BirthDate').data("DateTimePicker").date()){
                    errorMessage += '\"Date of birth\" ';
                    isEmptyField = true;
                }

                if(!profile.gender){
                    errorMessage += '\"Gender\" ';
                    isEmptyField = true;
                }

                // --------  Current Address ---------
                if(!address.streetAddress){
                    errorMessage += '\"Current Address\'s street address\" ';
                    isEmptyField = true;
                }

                if(!address.city){
                    errorMessage += '\"Current Address\'s city\" ';
                    isEmptyField = true;
                }

                if(!address.postcode){
                    errorMessage += '\"Current Address\'s postcode\" ';
                    isEmptyField = true;
                }

                if(!address.country){
                    errorMessage += '\"Current Address\'s country\" ';
                    isEmptyField = true;
                }

                // --------  Domicile Address ---------
                if(!domicile.streetAddress){
                    errorMessage += '\"Domicile Address\'s street address\"';
                    isEmptyField = true;
                }

                if(!domicile.city){
                    errorMessage += '\"Domicile Address\'s city\" ';
                    isEmptyField = true;
                }

                if(!domicile.postcode){
                    errorMessage += '\"domicile Address\'s postcode\" ';
                    isEmptyField = true;
                }

                if(!domicile.country){
                    errorMessage += '\"domicile Address\'s country\" ';
                    isEmptyField = true;
                }

                // ---------- Payroll details ---------
                if(!payroll.bankName){
                  errorMessage += '\"Bank Name\" ';
                  isEmptyField = true;
                }

                if(!payroll.holderName){
                  errorMessage += '\"Bank Holder Name\" ';
                  isEmptyField = true;
                }

                if(!payroll.accountNum){
                  errorMessage += '\"Account Number\" ';
                  isEmptyField = true;
                }

                // -------- Office related details --------

                if(!profile.position){
                    errorMessage += '\"Position\" ';
                    isEmptyField = true;
                }

                if(!profile.staffType){
                    errorMessage += '\"Staff Type\" ';
                    isEmptyField = true;
                }

                if(!$('#StartDate').data("DateTimePicker").date()){
                    errorMessage += '\"Start work date\" ';
                    isEmptyField = true;
                }

                if(!profile.phoneExtension){
                    errorMessage += '\"Phone extension\" ';
                    isEmptyField = true;
                }

                if(!department_id){
                    errorMessage += '\"Department\" ';
                    isEmptyField = true;
                }

                if(!office_id){
                    errorMessage += '\"Office location\" ';
                    isEmptyField = true;
                }

                if(isEmptyField === false){    //is any important field left empty?

                    if (profile.email !== verifyEmail) {
                  		logger.error('Email not matches!');
                  		return;
                  	}

                    profile.startWorkDate = $('#StartDate').data("DateTimePicker").date().format('YYYY-MM-DD');
                    profile.dob = $('#BirthDate').data("DateTimePicker").date().format('YYYY-MM-DD');

                    var username = (profile.email).split("@");  //only take the part before @ as username from email
                    profile.username = username[0];

                  	var objToSent = {Profile: profile, Address: address, Domicile: domicile, department_id: department_id, BankCard: payroll, Image: {image: ""}};

                    if ($('#Image').prop('files')[0]) {
                        var image = $('#Image').prop('files')[0];
                        var reader = new FileReader();
                        reader.addEventListener("load", function () {
                            objToSent.Image.image = reader.result;

                            console.log(objToSent);

                            return adminservice.createProfile(objToSent).then(function(data){
                                console.log(data);
                                if(data == null){
                                    logger.error('Unable to create a new profile!');
                                }else{
                                    logger.success('Profile created successfully!');
                                    $('#modalText').text('Account created! Default password is ' + data + '. Please change it on first login' );
                                    $('#accNotify').modal();
                                }
                            });
                        }, false);
                        reader.readAsDataURL(image);
                    } else {

                    }
                }else{
                    logger.error(errorMessage + " field can\'t be left empty!");
                    return;
                }
            }else{
                if(!profile && !address && !domicile){
                    logger.error("All fields are empty!");
                }else if(!profile){
                    logger.error("All profile's fields is empty!");
                }else if(!domicile){
                    logger.error("All domicile address's fields is empty!");
                }else{
                    logger.error("All current address's fields is empty!");
                }
            }
        }

		function toggleView(val) {
			vm.viewInfo = val;
		}

        function getProfilesByQuery(query) {
            return profileservice.getAllProfiles(query).then(function(data) {
                if (data == false) {

                } else {
                    vm.users = data.profiles;
                    setPaginationByQuery(data.count);
                }
            });
        }

        function setPaginationByQuery(count) {
            vm.lastPage = (count % 15 == 0) ? count/15 : Math.floor(count/15) + 1;
            vm.paginationList = [];
            for (var i=1; i<=vm.lastPage; i++) {
                vm.paginationList.push(i);
            }
        }

        function getNextProfilePageByQuery() {
            if (vm.currentPage < vm.lastPage) {
                getPagByQuery(vm.currentPage + 1);
            }
        }

        function getPreviousProfilePageByQuery() {
            if (vm.currentPage > 1) {
                getPageByQuery(vm.currentPage - 1);
            }
        }

        function getPageByQuery(i) {
            if (i == vm.currentPage) {
                console.log(vm.currentPage);
            } else if (i > 0 && i <= vm.lastPage) {
                vm.currentPage = i;
                vm.query.page = i;
                loadResources();
            } else {
                logger.error("[Error] Tried to load invalid page: " + i);
            }
        }

        function loadResources() {
            var promises = [getProfilesByQuery(vm.query)];
            return $q.all(promises).then(function() {});
        }
    }
})();
