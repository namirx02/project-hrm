(function() {
    'use strict';

    angular
        .module('app.admin')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'admin',
                config: {
                    url: '/',
                    templateUrl: 'app/admin/admin.html',
                    controller: 'AdminController',
                    controllerAs: 'vm',
                    title: 'admin',
                    hasChildren: true,
                    settings: {
                        nav: 1,
                        // content: '<i class="admin-logo"></i> Admin',
                        name: "ADMIN",
                        className: "admin-logo"
                    }
                }
            }
        ];
    }
})();
