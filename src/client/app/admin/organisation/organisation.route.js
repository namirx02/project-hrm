(function() {
    'use strict';

    angular
        .module('app.organisation')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'organisation',
                config: {
                    url: '/admin/organisation',
                    templateUrl: 'app/admin/organisation/organisation.html',
                    controller: 'OrganisationController',
                    controllerAs: 'vm',
                    title: 'organisation',
                    settings: {
                        nav: 2,
                        parent:'admin',
                        // content: '<i class="fa fa-globe" aria-hidden="true"></i> <span>Global Settings</span>'
                        name: "ORGANISATION",
                        className: "fa fa-th-list"
                    }
                }
            }
        ];
    }
})();
