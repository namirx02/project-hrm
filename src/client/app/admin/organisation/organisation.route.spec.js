/* jshint -W117, -W030 */
describe('organisation routes', function () {
    describe('state', function () {
        var view = 'app/organisation/organisation.html';

        beforeEach(function() {
            module('app.organisation', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state admin to url / ', function() {
            expect($state.href('organisation', {})).to.equal('/');
        });

        it('should map /admin route to admin View template', function () {
            expect($state.get('organisation').templateUrl).to.equal(view);
        });

        it('of admin should work with $state.go', function () {
            $state.go('organisation');
            $rootScope.$apply();
            expect($state.is('organisation'));
        });
    });
});
