	(function () {
    'use strict';

    angular
        .module('app.organisation', ['ngMaterial'])
        .controller('OrganisationController', OrganisationController);

    OrganisationController.$inject = ['$q', 'countryservice', 'profileservice', 'dataservice', 'officeservice', 'logger'];
    /* @ngInject */
    function OrganisationController($q, countryservice, profileservice, dataservice, officeservice, logger) {
        var vm = this;

        vm.title = 'Create Account';

        // Variables
        vm.departments = [];
        vm.availableDepts = [];
        vm.offices = [];
		vm.viewInfo = '1';
		vm.countries = [];

        // functions
		vm.toggleView = toggleView;
        vm.createOffice = createOffice;
        vm.createDepartment = createDepartment;
        vm.updateDepartmentSource = updateDepartmentSource;
        vm.queryTags = queryTags;

        activate();

        function activate() {
            var promises = [init(), getOffices(), getDepartments()];

            return $q.all(promises).then(function() {
                logger.info('Activated Create Account View');
            });
        }

        function init() {
            vm.countries = countryservice.countries();
        }

        function queryTags(query) {
            return profileservice.getProfilesByString({page: 1, string: query}).then(function(data) {
                var filteredNames = data === false ? [] : data;

                return $q.when(filteredNames);
            })
        }

        function getOffices(){
            return dataservice.getOffices().then(function (data){
                vm.offices = data;
            });
        }

        function getDepartments(){
            return dataservice.getDepartments().then(function (data){
                vm.departments = data;
            });
        }

        function updateDepartmentSource(office_id){
            vm.availableDepts = vm.departments.filter(function(item) {
                return item.office.id == office_id;
            });
        }

        function createOffice(office, address) {
            var objTosent = {"Office": office, "Address": address};
            return officeservice.createOffice(objTosent).then(function(data) {
                if (!data || data == 'fail') {
                    logger.error("Unable to create new office!");
                } else {
                    office.id = data;
                    office.address = address;
                    vm.offices.push(office);
                    logger.success("Office successfully created!")
                }
            });
        }

        function createDepartment(department) {
            var tempHead = {staffNum: department.dhead.staffNum, name: department.dhead.name, email: department.dhead.email};
            department.dhead = department.dhead.staffNum;
            return officeservice.createDepartment(department).then(function(data) {
                if (!data || data == 'fail') {
                    logger.error("Unable to create new department!");
                } else {
                    department.id = data;
                    department.dhead = tempHead;
                    vm.searchText = "";
                    for (var i = 0; i < vm.offices.length; i++) {
                        if (vm.offices[i].id == department.office.id) {
                            department.office = vm.offices[i];
                            break;
                        }
                    }
                    vm.departments.push(department);
                    vm.newdept = {};
                    logger.success("Department successfully created!")
                }
            });
        }

		function toggleView(val) {
			vm.viewInfo = val;
		}
    }
})();
