(function() {
    'use strict';

    angular.module('app.organisation', [
        'app.core',
        'app.widgets'
      ]);
})();
