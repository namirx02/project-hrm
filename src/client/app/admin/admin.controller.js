	(function () {
    'use strict';

    angular
        .module('app.profile')
        .controller('AdminController', AdminController);

    AdminController.$inject = ['$q', 'adminservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function AdminController($q, adminservice, dataservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'Admin';
        vm.createProfile = createProfile;
        
        activate();

        function activate() {
            var promises = [getOffices(), getDepartments()];
    		
            return $q.all(promises).then(function() {
            	console.log(vm.offices);
            	console.log(vm.departments);
                logger.info('Activated Admin View');
            });
        }
        
        function getOffices() {
            return dataservice.getOffices().then(function(data){
                if(!data){
                    logger.error("Cannot get Offices' details");
                }else{
                    vm.offices = data;
                }
            });
		}

        function getDepartments() {
            return dataservice.getDepartments().then(function(data){
                if(!data){
                    logger.error("Cannot get Departments' details");
                }else{
                    vm.departments = data;
                }
            });
		}
        
        function createProfile(profile, address, department_id, office_id, verifyEmail) {
        	if (profile.email !== verifyEmail) {
        		logger.error("Email not matches!");
        		return;
        	}
        	
        	profile.address = address.address + ', ' + address.city + ', ' + address.postcode + ', ' + address.country;
        	profile.password = "huijin_xinda123";
        	var objToSent = {Profile: profile, office_id: office_id, department_id: department_id};
        	
        	return adminservice.createProfile(objToSent).then(function(data){
                if(!data){
                    logger.error('There is an existing account with this email!');
                }else{
                    logger.success('Profile created successfully!');
                }
            });
        }
       
    }
})();
