/* jshint -W117, -W030 */
/* 'todolist routes' -> 'news routes' */
describe('news routes', function () {
    describe('state', function () {
		/* 'app/todolist/todolist.html' -> 'app/application/news/overtime_application.html' */
        var view = 'app/application/news/overtime_application.html';

		/* module('app.todolist', bard.fakeToastr) -> module('app.news', bard.fakeToastr); */
        beforeEach(function() {
            module('app.news', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

		/* 'todolist' -> 'news' */
        it('should map state news to url / ', function() {
            expect($state.href('news', {})).to.equal('/');
        });

        it('should map /news route to planner View template', function () {
            expect($state.get('news').templateUrl).to.equal(view);
        });

        it('of application should work with $state.go', function () {
            $state.go('news');
            $rootScope.$apply();
            expect($state.is('news'));
        });
    });
});
