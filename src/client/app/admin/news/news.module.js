(function() {
    'use strict';

	/* 'app.todolist' -> 'app.news' */
    angular.module('app.news', [
        'app.core',
        'app.widgets'
      ]);
})();
