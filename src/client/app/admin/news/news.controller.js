	(function () {
    'use strict';

	/* 'app.todolist' -> 'app.news'
	'TodolistController' -> ' 'NewsController' */
    angular
        .module('app.news', ['ui.tinymce', 'ngTagsInput', 'ngSanitize'])
        .controller('NewsController', NewsController);

    NewsController.$inject = ['$q', 'newsservice', 'profileservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function NewsController($q, newsservice, profileservice, dataservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'News management';

        // Variables
        vm.viewInfo = '1';
        vm.viewCreateNews = true;
        vm.viewPublished = false;
        vm.viewDraft = false;

        // functions
        vm.toggleView = toggleView;
        vm.queryTags = queryTags;
        vm.updateDepartmentSource = updateDepartmentSource;
        vm.publish = publish;
        vm.saveDraft = saveDraft;
        vm.showModal = showModal;
        vm.previewToggle = previewToggle;
        vm.deleteNews = deleteNews;
        vm.updateNews = updateNews;

        activate();

        function activate() {
            var promises = [getOffices(), getDepartments(), getPublished(), getDraft()];

            return $q.all(promises).then(function() {
                logger.info('Activated News View');
            });
        }

        function init() {
            vm.news = {};
            vm.restrict = {};
        }

        function toggleView(val) {
            vm.viewCreateNews = val == '1' ? true : false;
            vm.viewPublished = val == '2' ? true : false;
            vm.viewDraft = val == '3' ? true : false;
        }

        function queryTags(query) {
            return profileservice.getProfilesByString({page: 1, string: query}).then(function(data) {
                var filteredNames = data === false ? [] : data;

                for (var i = 0; i < filteredNames.length; i++) {
                    filteredNames[i].name += " (" + filteredNames[i].username + ")";
                }
                return $q.when(filteredNames);
            })
        }

        function getPublished() {
            return newsservice.getPublished().then(function(data) {
               if (data === false)
                   logger.error("Unable to retrieve published articles");
               else
                   vm.publishedArticles = data;
            });
        }

        function getDraft() {
            return newsservice.getDraft().then(function(data) {
                if (data === false)
                    logger.error("Unable to retrieve draft articles");
                else
                    vm.draftArticles = data;
            });
        }

        function getOffices(){
            return dataservice.getOffices().then(function (data){
                vm.offices = data;
            });
        }

        function getDepartments(){
            return dataservice.getDepartments().then(function (data){
                vm.departments = data;
            });
        }

        function updateDepartmentSource(office_id){
            vm.availableDepts = vm.departments.filter(function(item) {
                return item.office.id == office_id;
            });
        }

        function publish(news, restrict) {
            news.status = 'published';
            news = formatViewable(news, restrict);

            return newsservice.postNews(news).then(function (data) {
                if (data === false) {
                    logger.error("Unable to publish news article");
                } else {
                    news = data;
                    vm.publishedArticles.push(news);
                    init();
                    logger.success("News article successfully published!");
                }
            });
        }

        function saveDraft(news, restrict) {
            news.status = 'draft';
            news = formatViewable(news, restrict);

            return newsservice.postNews(news).then(function (data) {
                if (data === false) {
                    logger.error("Unable to save news article");
                } else {
                    logger.success("News article successfully saved!");
                }
            });
        }

        function updateNews(news, restrict, status) {
            var oldStatus = news.status;
            news.status = status;
            news = formatViewable(news, restrict);

            return newsservice.updateNews(news).then(function (data) {
                if (data === false) {
                    logger.error("Unable to update news article");
                } else {
                    if (oldStatus != status) {
                        if (oldStatus == "published") {
                            moveNews(news, vm.publishedArticles, vm.draftArticles);
                        } else {
                            moveNews(news, vm.draftArticles, vm.publishedArticles);
                        }
                    }
                    init();
                    logger.success("News article successfully updated!");
                }
            });
        }

        function moveNews(news, from, to) {
            from.splice(from.indexOf(news), 1);
            to.push(news);
        }

        function formatViewable(news, restrict) {
            switch (news.viewable) {
                case 'office':
                    news.office_id = restrict.office;
                    break;
                case 'department':
                    news.department_id = restrict.department;
                    break;
                case 'custom':
                    news.profiles = restrict.profiles;
                    break;
                default:
                    break;
            }

            return news;
        }

        function showModal(news) {
            vm.modalNews = news;
            vm.modalViewEdit = true;
            if (vm.modalNews.viewable == "department") {
                updateDepartmentSource(vm.modalNews.office_id);
            }

            console.log(news);
            vm.restrict = {profiles: news.profiles ? news.profiles : []};
            $('#newsModal').modal();
        }

        function previewToggle() {
            vm.modalViewEdit = vm.modalViewEdit ? false : true;
        }

        function deleteNews(news) {
            return newsservice.deleteNews(news.id).then(function (data) {
               if (data === false) {
                   logger.error("Unable to delete news article!");
               } else {
                   $('#newsModal').modal('toggle');
                   if (news.status == "published") {
                       vm.publishedArticles.splice(vm.publishedArticles.indexOf(news), 1);
                   } else {
                       vm.draftArticles.splice(vm.draftArticles.indexOf(news), 1);
                   }
                   logger.success("News article deleted successfully!");
               }
            });
        }
    }
})();
