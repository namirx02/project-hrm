(function() {
    'use strict';

	/* 'app.todolist -> 'app.news' */
    angular
        .module('app.news')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'news',
                config: {
                    url: '/admin/news',
                    templateUrl: 'app/admin/news/news.html',
                    controller: 'NewsController',
                    controllerAs: 'vm',
                    title: 'News',
                    settings: {
                        nav: 2,
                        parent: 'admin',
                        // content: '<i class="fa fa-newspaper-o"></i> <span>News</span>'
                        name: "NEWS",
                        className: "fa fa-newspaper-o"
                    }
                }
            }
        ];
    }
})();
