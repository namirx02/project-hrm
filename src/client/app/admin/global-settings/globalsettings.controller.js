	(function () {
    'use strict';

    angular
        .module('app.globalsettings')
        .controller('GlobalsettingsController', GlobalsettingsController);

    GlobalsettingsController.$inject = ['$q', 'countryservice', 'adminservice', 'dataservice', 'officeservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function GlobalsettingsController($q, countryservice, adminservice, dataservice, officeservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'Create Account';

        // Variables
        vm.departments = [];
        vm.availableDepts = [];
        vm.offices = [];
		vm.viewInfo = '1';
		vm.viewNewAccount = true;
		vm.viewOffice = false;
		vm.viewDepartment = false;

		vm.countries = [];

        // functions
        vm.createProfile = createProfile;
		vm.toggleView = toggleView;
        vm.createOffice = createOffice;
        vm.createDepartment = createDepartment;
        vm.updateDepartmentSource = updateDepartmentSource;

        activate();

        function activate() {
            var promises = [init(), getOffices(), getDepartments()];

            return $q.all(promises).then(function() {
                logger.info('Activated Create Account View');
            });
        }

        function init() {

        }

        function getOffices(){
            return dataservice.getOffices().then(function (data){
                vm.offices = data;
            });
        }

        function getDepartments(){
            return dataservice.getDepartments().then(function (data){
                vm.departments = data;
            });
        }

        function updateDepartmentSource(office_id){
            vm.availableDepts = vm.departments.filter(function(item) {
               return item.office.id == office_id;
            });
        }

        function createOffice(office, address) {
            var objTosent = {"Office": office, "Address": address};
            return officeservice.createOffice(objTosent).then(function(data) {
                if (!data || data == 'fail') {
                    logger.error("Unable to create new office!");
                } else {
                    office.id = data;
                    office.address = address;
                    vm.offices.push(office);
                    logger.success("Office successfully created!")
                }
            });
        }

        function createDepartment(department) {
            console.log(department);
            return officeservice.createDepartment(department).then(function(data) {
                if (!data || data == 'fail') {
                    logger.error("Unable to create new department!");
                } else {
                    department.id = data;
                    vm.departments.push(department);
                    logger.success("Department successfully created!")
                }
            });
        }

        function createProfile(profile, address, domicile, department_id, office_id, payroll, verifyEmail) {

            var isEmptyField = false;
            var errorMessage = '';


            if(profile && address && domicile){     //check if profile object exists or address object exists or not (It does not exist if all of the fields of an object is empty)

                // check for empty field
                // --------- Personal Details ---------
                if(!profile.name){
                    errorMessage += '\"Last name\" ';
                    isEmptyField = true;
                }

                if(!profile.email){
                    errorMessage += '\"Email\" ';
                    isEmptyField = true;
                }

                if(!$('#BirthDate').data("DateTimePicker").date()){
                    errorMessage += '\"Date of birth\" ';
                    isEmptyField = true;
                }

                if(!profile.gender){
                    errorMessage += '\"Gender\" ';
                    isEmptyField = true;
                }

                // --------  Current Address ---------
                if(!address.streetAddress){
                    errorMessage += '\"Current Address\'s street address\" ';
                    isEmptyField = true;
                }

                if(!address.city){
                    errorMessage += '\"Current Address\'s city\" ';
                    isEmptyField = true;
                }

                if(!address.postcode){
                    errorMessage += '\"Current Address\'s postcode\" ';
                    isEmptyField = true;
                }

                if(!address.country){
                    errorMessage += '\"Current Address\'s country\" ';
                    isEmptyField = true;
                }

                // --------  Domicile Address ---------
                if(!domicile.streetAddress){
                    errorMessage += '\"Domicile Address\'s street address\"';
                    isEmptyField = true;
                }

                if(!domicile.city){
                    errorMessage += '\"Domicile Address\'s city\" ';
                    isEmptyField = true;
                }

                if(!domicile.postcode){
                    errorMessage += '\"domicile Address\'s postcode\" ';
                    isEmptyField = true;
                }

                if(!domicile.country){
                    errorMessage += '\"domicile Address\'s country\" ';
                    isEmptyField = true;
                }

                // ---------- Payroll details ---------
                if(!payroll.bankName){
                  errorMessage += '\"Bank Name\" ';
                  isEmptyField = true;
                }

                if(!payroll.holderName){
                  errorMessage += '\"Bank Holder Name\" ';
                  isEmptyField = true;
                }

                if(!payroll.accountNum){
                  errorMessage += '\"Account Number\" ';
                  isEmptyField = true;
                }

                // -------- Office related details --------

                if(!profile.position){
                    errorMessage += '\"Position\" ';
                    isEmptyField = true;
                }

                if(!profile.staffType){
                    errorMessage += '\"Staff Type\" ';
                    isEmptyField = true;
                }

                if(!$('#StartDate').data("DateTimePicker").date()){
                    errorMessage += '\"Start work date\" ';
                    isEmptyField = true;
                }

                if(!profile.phoneExtension){
                    errorMessage += '\"Phone extension\" ';
                    isEmptyField = true;
                }

                if(!department_id){
                    errorMessage += '\"Department\" ';
                    isEmptyField = true;
                }

                if(!office_id){
                    errorMessage += '\"Office location\" ';
                    isEmptyField = true;
                }

                if(isEmptyField === false){    //is any important field left empty?

                    if (profile.email !== verifyEmail) {
                  		logger.error('Email not matches!');
                  		return;
                  	}

                    profile.startWorkDate = $('#StartDate').data("DateTimePicker").date().format('YYYY-MM-DD');
                    profile.dob = $('#BirthDate').data("DateTimePicker").date().format('YYYY-MM-DD');

                    var username = (profile.email).split("@");  //only take the part before @ as username from email
                    profile.username = username[0];

                  	var objToSent = {Profile: profile, Address: address, Domicile: domicile, department_id: department_id, BankCard: payroll, Image: {image: ""}};

                    if ($('#Image').prop('files')[0]) {
                        var image = $('#Image').prop('files')[0];
                        var reader = new FileReader();
                        reader.addEventListener("load", function () {
                            objToSent.Image.image = reader.result;

                            console.log(objToSent);

                            return adminservice.createProfile(objToSent).then(function(data){
                                console.log(data);
                                if(data == null){
                                    logger.error('Unable to create a new profile!');
                                }else{
                                    logger.success('Profile created successfully!');
                                    $('#modalText').text('Account created! Default password is ' + data + '. Please change it on first login' );
                                    $('#accNotify').modal();
                                }
                            });
                        }, false);
                        reader.readAsDataURL(image);
                    } else {

                    }
                }else{
                    logger.error(errorMessage + " field can\'t be left empty!");
                    return;
                }
            }else{
                if(!profile && !address && !domicile){
                    logger.error("All fields are empty!");
                }else if(!profile){
                    logger.error("All profile's fields is empty!");
                }else if(!domicile){
                    logger.error("All domicile address's fields is empty!");
                }else{
                    logger.error("All current address's fields is empty!");
                }
            }
        }

		function toggleView(val) {
			vm.viewNewAccount = val == '1' ? true : false;
			vm.viewOffice = val == '2' ? true : false;
			vm.viewSettings = val == '3' ? true : false;
		}
    }
})();
