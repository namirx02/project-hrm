(function() {
    'use strict';

    angular
        .module('app.globalsettings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'globalsettings',
                config: {
                    url: '/admin/global-settings',
                    templateUrl: 'app/admin/global-settings/globalsettings.html',
                    controller: 'GlobalsettingsController',
                    controllerAs: 'vm',
                    title: 'globalsettings',
                    settings: {
                        nav: 2,
                        parent:'admin',
                        // content: '<i class="fa fa-globe" aria-hidden="true"></i> <span>Global Settings</span>'
                        name: "GLOBAL_SETTINGS",
                        className: "fa fa-globe"
                    }
                }
            }
        ];
    }
})();
