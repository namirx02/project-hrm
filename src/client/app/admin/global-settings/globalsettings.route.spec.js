/* jshint -W117, -W030 */
describe('globalsettings routes', function () {
    describe('state', function () {
        var view = 'app/global-settings/globalsettings.html';

        beforeEach(function() {
            module('app.globalsettings', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state admin to url / ', function() {
            expect($state.href('globalsettings', {})).to.equal('/');
        });

        it('should map /admin route to admin View template', function () {
            expect($state.get('globalsettings').templateUrl).to.equal(view);
        });

        it('of admin should work with $state.go', function () {
            $state.go('globalsettings');
            $rootScope.$apply();
            expect($state.is('globalsettings'));
        });
    });
});
