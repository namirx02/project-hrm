(function() {
    'use strict';

    angular.module('app.globalsettings', [
        'app.core',
        'app.widgets'
      ]);
})();
