	(function () {
    'use strict';

    angular
        .module('app.dashboard', ['ngSanitize'])
        .filter('summary', function () {
            return function (text, length) {
                if (text.length > length) {
                    return text.substr(0, length) + "...";
                }
                return text;
            }
        })
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$q', 'dataservice', 'newsservice', 'logger'];
    /* @ngInject */
    function DashboardController($q, dataservice, newsservice, logger) {
        var vm = this;

        vm.title = 'Dashboard';
        vm.offset = 0;
        vm.hasMore = true;

        vm.loadMore = loadMore;

        activate();

        function activate() {
            if (sessionStorage.getItem('user')) {
                vm.loginUser = JSON.parse(sessionStorage.getItem('user'));
                vm.loggedIn = true;
            }

            var promises = [init(), getNews(), dataservice.resizePage()];
            return $q.all(promises).then(function() {
                logger.info('Activated Dashboard View');
            });
        }

        function init() {
            vm.offset = 0;
            vm.hasMore = true;
            setTimeout(function() {
                $(function () {
                    //-------------
                    //- PIE CHART -
                    //-------------
                    // Get context with jQuery - using jQuery's .get() method.
                    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
                    var pieChart = new Chart(pieChartCanvas);
                    var PieData = [
                        {
                            value: 700,
                            color: "#f56954",
                            highlight: "#f56954",
                            label: "Chrome"
                        },
                        {
                            value: 500,
                            color: "#00a65a",
                            highlight: "#00a65a",
                            label: "IE"
                        },
                        {
                            value: 400,
                            color: "#f39c12",
                            highlight: "#f39c12",
                            label: "FireFox"
                        },
                        {
                            value: 400,
                            color: "#f012be",
                            highlight: "#f012be",
                            label: "Parental"
                        },
                        {
                            value: 600,
                            color: "#00c0ef",
                            highlight: "#00c0ef",
                            label: "Safari"
                        },
                        {
                            value: 300,
                            color: "#3c8dbc",
                            highlight: "#3c8dbc",
                            label: "Opera"
                        },
                        {
                            value: 100,
                            color: "#d2d6de",
                            highlight: "#d2d6de",
                            label: "Navigator"
                        }
                    ];
                    var pieOptions = {
                        //Boolean - Whether we should show a stroke on each segment
                        segmentShowStroke: true,
                        //String - The colour of each segment stroke
                        segmentStrokeColor: "#fff",
                        //Number - The width of each segment stroke
                        segmentStrokeWidth: 2,
                        //Number - The percentage of the chart that we cut out of the middle
                        percentageInnerCutout: 50, // This is 0 for Pie charts
                        //Number - Amount of animation steps
                        animationSteps: 100,
                        //String - Animation easing effect
                        animationEasing: "easeOutBounce",
                        //Boolean - Whether we animate the rotation of the Doughnut
                        animateRotate: true,
                        //Boolean - Whether we animate scaling the Doughnut from the centre
                        animateScale: false,
                        //Boolean - whether to make the chart responsive to window resizing
                        responsive: true,
                        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                        maintainAspectRatio: true,
                        //String - A legend template
                        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
                    };
                    //Create pie or douhnut chart
                    // You can switch between pie and douhnut using the method below.
                    pieChart.Doughnut(PieData, pieOptions);

                });
            }, 1);
        }

        function getNews() {
            return newsservice.getNews(vm.offset, 5, 'timeCreated').then(function(data) {
               if (data === false) {
                   logger.error("Unable to retrieve news articles!");
               } else {
                   if (data.hasMore < 5)
                       vm.hasMore = false;
                   vm.articles = data;
               }
            });
        }

        function loadMore() {
            vm.offset += 5;
            return newsservice.getNews(vm.offset, 5, 'timeCreated').then(function(data) {
                if (data === false) {
                    logger.error("Unable to retrieve news articles!");
                } else {
                    if (data.length == 0 || data.length < 5) {
                        vm.hasMore = false;
                    }
                    vm.articles.push.apply(vm.articles, data);
                }
            });
        }
    }
})();
