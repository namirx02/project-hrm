	(function () {
    'use strict';

    angular
        .module('app.application')
        .controller('ApplicationController', ApplicationController);

    ApplicationController.$inject = ['$q', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function ApplicationController($q, dataservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'Application';

        activate();

        function activate() {
            var promises = [];
            return $q.all(promises).then(function() {
                logger.info('Activated Application View');
            });
        }
    }
})();
