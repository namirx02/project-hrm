(function() {
    'use strict';

    angular
        .module('app.applicationhistory')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Applicationhistory',
                config: {
                    url: '/applications/history',
                    templateUrl: 'app/applications/applicationhistory/application_history.html',
                    controller: 'ApplicationHistoryController',
                    controllerAs: 'vm',
                    title: 'Applicationhistory',
                    hideFromSidebar: true,
                    settings: {
                        nav: 2,
						parent: 'Application',
                        // content: '<i class="application-history-logo"></i> <span>Application History</span>'
                        name: "APPLICATION_HISTORY",
                        className: "application-history-logo"
                    }
                }
            }
        ];
    }
})();
