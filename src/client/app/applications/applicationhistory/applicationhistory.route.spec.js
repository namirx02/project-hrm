/* jshint -W117, -W030 */
describe('applicationhistory routes', function () {
    describe('state', function () {
        var view = 'app/application/applicationhistory/application_history.html';

        beforeEach(function() {
            module('app.applicationhistory', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state applicationhistory to url / ', function() {
            expect($state.href('applicationhistory', {})).to.equal('/');
        });

        it('should map /applicationhistory route to applicationhistory View template', function () {
            expect($state.get('applicationhistory').templateUrl).to.equal(view);
        });

        it('of applicationhistory should work with $state.go', function () {
            $state.go('applicationhistory');
            $rootScope.$apply();
            expect($state.is('applicationhistory'));
        });
    });
});
