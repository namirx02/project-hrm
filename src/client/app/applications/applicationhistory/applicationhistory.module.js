(function() {
    'use strict';

    angular.module('app.applicationhistory', [
        'app.core',
        'app.widgets'
      ]);
})();
