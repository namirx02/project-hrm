	(function () {
    'use strict';

    angular
        .module('app.applicationhistory')
        .controller('ApplicationHistoryController', ApplicationHistoryController);

    ApplicationHistoryController.$inject = ['$q', 'applicationservice', 'logger'];
    /* @ngInject */
    function ApplicationHistoryController($q, applicationservice, logger) {
        var vm = this;

        vm.title = 'Application History';
        vm.source = ['Leave application', 'Overtime application', 'Expense application'];

        vm.query = {startDate: '', endDate: '', individual: true};
        vm.types = [];
        vm.pendings = [];
        vm.revieweds = [];
        vm.viewPending = true;

        vm.toggleView = toggleView;
        vm.filter = filter;
        vm.queryTags = queryTags;

        // pagination
        vm.currentPage = 1;
        vm.lastPage = 1;
        vm.paginationList = [];

        vm.getNextProfilePage = getNextProfilePage;
        vm.getPreviousProfilePage = getPreviousProfilePage;
        vm.getPage = getPage;

        activate();

        function activate() {
            var promises = [init(), getPendings(vm.currentPage, 'date', vm.query, vm.types), getRevieweds(vm.currentPage, 'date', vm.query, vm.types)];
            return $q.all(promises).then(function() {
                logger.info('Activated Application History View');
            });
        }

        function filter() {
            if($('#application_history_filter_date_from').data("DateTimePicker").date()) {
                vm.query.startDate = $('#application_history_filter_date_from').data("DateTimePicker").date().format('YYYY-MM-DD');
            }
            if($('#application_history_filter_date_to').data("DateTimePicker").date()) {
                vm.query.endDate = $('#application_history_filter_date_to').data("DateTimePicker").date().format('YYYY-MM-DD');
            }

            activate();
        }

        function init() {
            vm.pendings = [];
            vm.revieweds = [];
            vm.currentPage = 1;
            vm.lastPage = 1;
        }

        function queryTags(query) {
            return vm.source.filter(function(item) {
                return item.toLowerCase().indexOf(query.toLowerCase()) != -1;
            });
        }

        function getPendings(page, sortBy, query, types) {
            return applicationservice.getPendingApplications(page, sortBy, query, types).then(function(data) {
                if (data === false) {
                    logger.error("Pending applications are not available!");
                } else {
                    vm.pendings = data.submissions;
                    vm.pendings_count = data.count;

                    for (var i = 0; i < vm.pendings.length; i++) {
                        vm.pendings[i].startDate = vm.pendings[i].startDate.substr(0,12).trim();
                        vm.pendings[i].endDate = vm.pendings[i].endDate.substr(0,12).trim();
                    }

                    if (vm.viewPending) {
                        setPagination(vm.pendings_count);
                    }
                }
            });
        }

        function getRevieweds(page, sortBy, query, types) {
            return applicationservice.getReviewedApplications(page, sortBy, query, types).then(function(data) {
                if (data === false) {
                    logger.error("Pending applications are not available!");
                } else {
                    vm.revieweds = data.submissions;
                    vm.revieweds_count = data.count;

                    for (var i = 0; i < vm.revieweds.length; i++) {
                        vm.revieweds[i].startDate = vm.revieweds[i].startDate.substr(0,12).trim();
                        vm.revieweds[i].endDate = vm.revieweds[i].endDate.substr(0,12).trim();
                    }

                    if (!vm.viewPending) {
                        setPagination(vm.revieweds_count);
                    }
                }
            });
        }

        function toggleView(tab) {
            if (tab == 'pending-application') {
                vm.viewPending =  true;
                setPagination(vm.pendings_count);
            } else {
                vm.viewPending = false;
                setPagination(vm.revieweds_count);
            }
        }

        /*
         Sets the pagination and its parameters when a search is used
         Makes sure that clicking a page will return the correct results
         */
        function setPagination(count) {
            vm.lastPage = (count % 30 == 0) ? count/30 : Math.floor(count/30) + 1;
            vm.paginationList = [];
            for (var i=1; i<=vm.lastPage; i++) {
                vm.paginationList.push(i);
            }
        }

        function getNextProfilePage() {
            if (vm.currentPage < vm.lastPage) {
                getPage(vm.currentPage + 1);
            }
        }

        function getPreviousProfilePage() {
            if (vm.currentPage > 1) {
                getPage(vm.currentPage - 1);
            }
        }

        function getPage(i) {
            if (i == vm.currentPage) {
                console.log(vm.currentPage);
            } else if (i > 0 && i <= vm.lastPage) {
                vm.currentPage = i;
                loadResources(i);
            } else {
                logger.error("[Error] Tried to load invalid page: " + i);
            }
        }

        function loadResources(i) {
            var promises = [getPendings(i, 'date', vm.query, vm.types), getRevieweds(i, 'date', vm.query, vm.types)];
            return $q.all(promises).then(function() {});
        }
    }
})();
