/* jshint -W117, -W030 */
describe('ApplicationHistoryController', function() {
    var controller;
    var people = mockData.getMockPeople();

    beforeEach(function() {
        bard.appModule('app.applicationhistory');
        bard.inject('$controller', '$log', '$q', '$rootScope', 'dataservice');
    });

	/* might want to change 'getPeople' and the data returned to fit application history */
    beforeEach(function () {
        sinon.stub(dataservice, 'getPeople').returns($q.when(people));
        controller = $controller('ApplicationHistoryController');
        $rootScope.$apply();
    });

    bard.verifyNoOutstandingHttpRequests();

    describe('Application History controller', function() {
        it('should be created successfully', function () {
            expect(controller).to.be.defined;
        });

		/* some section may require modification */
        describe('after activate', function() {
            it('should have title of Application History', function () {
                expect(controller.title).to.equal('Profile');
            });

            it('should have logged "Activated"', function() {
                expect($log.info.logs).to.match(/Activated/);
            });

            it('should have news', function () {
                expect(controller.news).to.not.be.empty;
            });

            it('should have at least 1 item', function () {
                expect(controller.people).to.have.length.above(0);
            });

            it('should have people count of 5', function () {
                expect(controller.people).to.have.length(7);
            });
        });
    });
});
