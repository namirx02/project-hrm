(function() {
    'use strict';

    angular
        .module('app.application')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Application',
                config: {
                    url: '/applications',
                    templateUrl: 'app/applications/application.html',
                    controller: 'ApplicationController',
                    controllerAs: 'vm',
                    title: 'Application',
                    hasChildren: true,
                    settings: {
                        nav: 1,
                        // content: '<i class="application-logo"></i> <span>Application Form</span>'
                        name: "APPLICATION_FORM",
                        className: "application-logo"
                    }
                }
            }
        ];
    }
})();
