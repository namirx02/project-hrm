/* jshint -W117, -W030 */
/* 'todolist routes' -> 'overtimeapplication routes' */
describe('overtimeapplication routes', function () {
    describe('state', function () {
		/* 'app/todolist/todolist.html' -> 'app/application/overtimeapplication/overtime_application.html' */
        var view = 'app/application/overtimeapplication/overtime_application.html';

		/* module('app.todolist', bard.fakeToastr) -> module('app.overtimeapplication', bard.fakeToastr); */
        beforeEach(function() {
            module('app.overtimeapplication', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

		/* 'todolist' -> 'overtimeapplication' */
        it('should map state overtimeapplication to url / ', function() {
            expect($state.href('overtimeapplication', {})).to.equal('/');
        });

        it('should map /overtimeapplication route to planner View template', function () {
            expect($state.get('overtimeapplication').templateUrl).to.equal(view);
        });

        it('of application should work with $state.go', function () {
            $state.go('overtimeapplication');
            $rootScope.$apply();
            expect($state.is('overtimeapplication'));
        });
    });
});
