(function() {
    'use strict';

	/* 'app.todolist -> 'app.overtimeapplication' */
    angular
        .module('app.overtimeapplication')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Overtimeapplication',
                config: {
                    url: '/applications/overtime',
                    templateUrl: 'app/applications/overtimeapplication/overtime_application.html',
                    controller: 'OvertimeapplicationController',
                    controllerAs: 'vm',
                    title: 'Overtimeapplication',
                    settings: {
                        nav: 2,
                        parent: 'Application',
                        // content: '<i class="overtime-application-logo"></i> <span>Overtime Application</span>'
                        name: "OVERTIME_APPLICATION",
                        className: "overtime-application-logo"
                    }
                }
            }
        ];
    }
})();
