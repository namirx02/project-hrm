(function() {
    'use strict';

	/* 'app.todolist' -> 'app.overtimeapplication' */
    angular.module('app.overtimeapplication', [
        'app.core',
        'app.widgets'
      ]);
})();
