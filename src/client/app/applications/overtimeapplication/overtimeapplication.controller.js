	(function () {
    'use strict';

	/* 'app.todolist' -> 'app.overtimeapplication'
	'TodolistController' -> ' 'OvertimeapplicationController' */
    angular
        .module('app.overtimeapplication')
        .controller('OvertimeapplicationController', OvertimeapplicationController);

    OvertimeapplicationController.$inject = ['$q', 'applicationservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function OvertimeapplicationController($q, applicationservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

		/* 'To-do List' -> 'Overtime Application' */
        vm.title = 'Overtime Application';
        vm.overtime = {};
        vm.submitOvertime = submitOvertime;
        vm.optionToDate = false;

        activate();

        function activate() {
            var promises = [init()];
            return $q.all(promises).then(function() {
                logger.info('Activated Overtime Application View');
            });
        }

        function init() {
            setTimeout(
                function loadScript() {
                    $(function() {
                        $('#overtime_date').datetimepicker({
                            format: 'DD/MM/YYYY',
                        });

                        $('#overtime_date_optional_to').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                    });
                }
            );
        }
        function submitOvertime() {
            var isEmptyField = false;
            var errorMessage = '';

            if(!$('#overtime_date').data("DateTimePicker").date()){
                errorMessage += '\"Date of overtime\" ';
                isEmptyField = true;
            }
            if(vm.overtime.reason == '' || vm.overtime.reason == undefined){
                errorMessage += '\"Reason of overtime\" ';
                isEmptyField = true;
            }

            if (!isEmptyField) {
                vm.overtime.startDate = $('#overtime_date').data("DateTimePicker").date().format('YYYY-MM-DD');
                if (vm.optionToDate)
                    vm.overtime.endDate = $('#overtime_date_optional_to').data("DateTimePicker").date().format('YYYY-MM-DD');
                else
                    vm.overtime.endDate = vm.overtime.startDate;
                return applicationservice.applyOvertime({"Overtime": vm.overtime}).then(function(data) {
                    if (data === false)
                        logger.error("Unable to submit overtime request!");
                    else {
                        vm.overtime = {};
                        logger.success('Overtime request submitted!')
                    }
                });
            } else{
                logger.error(errorMessage + " field can\'t be left empty!");
                return;
            }

        }
    }
})();
