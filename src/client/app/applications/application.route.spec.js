/* jshint -W117, -W030 */
describe('application routes', function () {
    describe('state', function () {
        var view = 'app/application/application.html';

        beforeEach(function() {
            module('app.application', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state application to url / ', function() {
            expect($state.href('application', {})).to.equal('/');
        });

        it('should map /application route to application View template', function () {
            expect($state.get('application').templateUrl).to.equal(view);
        });

        it('of application should work with $state.go', function () {
            $state.go('application');
            $rootScope.$apply();
            expect($state.is('application'));
        });
    });
});
