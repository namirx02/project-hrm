(function() {
    'use strict';

	/* 'app.todolist' -> 'app.applicationreview' */
    angular.module('app.applicationreview', [
        'app.core',
        'app.widgets'
      ]);
})();
