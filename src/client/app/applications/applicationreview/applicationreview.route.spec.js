/* jshint -W117, -W030 */
/* 'todolist routes' -> 'applicationreview routes' */
describe('applicationreview routes', function () {
    describe('state', function () {
		/* 'app/todolist/todolist.html' -> 'app/application/applicationreview/application_review.html' */
        var view = 'app/application/applicationreview/application_review.html';

		/* module('app.todolist', bard.fakeToastr) -> module('app.applicationreview', bard.fakeToastr); */
        beforeEach(function() {
            module('app.applicationreview', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

		/* 'todolist' -> 'applicationreview' */
        it('should map state applicationreview to url / ', function() {
            expect($state.href('applicationreview', {})).to.equal('/');
        });

        it('should map /applicationreview route to planner View template', function () {
            expect($state.get('applicationreview').templateUrl).to.equal(view);
        });

        it('of application should work with $state.go', function () {
            $state.go('applicationreview');
            $rootScope.$apply();
            expect($state.is('applicationreview'));
        });
    });
});
