(function() {
    'use strict';

	/* 'app.todolist -> 'app.applicationreview' */
    angular
        .module('app.applicationreview')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Applicationreview',
                config: {
                    url: '/applications/review',
                    templateUrl: 'app/applications/applicationreview/application_review.html',
                    controller: 'ApplicationreviewController',
                    controllerAs: 'vm',
                    title: 'Applicationreview',
                    settings: {
                        nav: 2,
                        parent: 'Application',
                        // content: '<i class="application-review-logo"></i> <span>Application Review</span>'
                        name: "APPLICATION_REVIEW",
                        className: "application-review-logo"
                    }
                }
            }
        ];
    }
})();
