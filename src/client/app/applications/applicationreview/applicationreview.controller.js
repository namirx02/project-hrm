	(function () {
    'use strict';

	/* 'app.todolist' -> 'app.applicationreview'
	'TodolistController' -> ' 'ApplicationreviewController' */
    angular
        .module('app.applicationreview', ['ngTagsInput'])
        .controller('ApplicationreviewController', ApplicationreviewController);

    ApplicationreviewController.$inject = ['$q', 'applicationservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function ApplicationreviewController($q, applicationservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

		/* 'To-do List' -> 'Application Review' */
        vm.title = 'Application Review';
        vm.source = ['Leave application', 'Overtime application', 'Expense application'];
        vm.query = {startDate: '', endDate: '', individual: false};
        vm.types = [];
        vm.viewPending = true;
        vm.checkMaster = false;
        vm.checkedList = [];
        vm.boxes = [];
        vm.pendings = [];
        vm.revieweds = [];

        vm.checkedCounter = 0;

        vm.handleApplications = handleApplications;
        vm.filter = filter;
        vm.updateCheckedList = updateCheckedList;
        vm.toggleView = toggleView;
        vm.queryTags = queryTags;

        // pagination
        vm.currentPage = 1;
        vm.lastPage = 1;
        vm.paginationList = [];

        vm.getNextProfilePage = getNextProfilePage;
        vm.getPreviousProfilePage = getPreviousProfilePage;
        vm.getPage = getPage;

        activate();

        function activate() {
            var promises = [init(), getPendings(vm.currentPage, 'date', vm.query, vm.types), getRevieweds(vm.currentPage, 'date', vm.query, vm.types), addon()];
            return $q.all(promises).then(function() {
                logger.info('Activated Application Review View');
            });
        }

        function addon() {
            setTimeout(function() {
                $(function() {
                    $('#application_review_filter_date_from').datetimepicker({
                        format: 'DD/MM/YYYY',
                    });

                    $('#application_review_filter_date_to').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });

                });
            }, 1);
        }

        function filter() {
            if($('#application_review_filter_date_from').data("DateTimePicker").date()) {
                vm.query.startDate = $('#application_review_filter_date_from').data("DateTimePicker").date().format('YYYY-MM-DD');
            }
            if($('#application_review_filter_date_to').data("DateTimePicker").date()) {
                vm.query.endDate = $('#application_review_filter_date_to').data("DateTimePicker").date().format('YYYY-MM-DD');
            }


            activate();
        }

        function queryTags(query) {
            return vm.source.filter(function(item) {
               return item.toLowerCase().indexOf(query.toLowerCase()) != -1;
            });
        }

        function init() {
            vm.checkMaster = false;
            vm.checkedCounter = 0;
            vm.checkedList = [];
            vm.boxes = [];
            vm.pendings = [];
            vm.revieweds = [];
            vm.currentPage = 1;
            vm.lastPage = 1;
        }

        function getPendings(page, sortBy, query, types) {
            return applicationservice.getPendingApplications(page, sortBy, query, types).then(function(data) {
                if (data === false) {
                    logger.error("Pending applications are not available!");
                } else {
                    vm.pendings = data.submissions;
                    vm.pendings_count = data.count;

                    for (var i = 0; i < vm.pendings.length; i++) {
                        vm.pendings[i].startDate = vm.pendings[i].startDate.substr(0,12).trim();
                        vm.pendings[i].endDate = vm.pendings[i].endDate.substr(0,12).trim();
                        vm.boxes[vm.pendings[i].id] = false;
                        vm.checkedList.push({
                            id: vm.pendings[i].id,
                            value: false,
                            submittedDate: vm.pendings[i].submittedDate,
                            ownerID: vm.pendings[i].profile.staffNum
                        });
                    }

                    if (vm.viewPending) {
                        setPagination(vm.pendings_count);
                    }
                }
            });
        }

        function getRevieweds(page, sortBy, query, types) {
            return applicationservice.getReviewedApplications(page, sortBy, query, types).then(function(data) {
                if (data === false) {
                    logger.error("Reviewed applications are not available!");
                } else {
                    vm.revieweds = data.submissions;
                    vm.revieweds_count = data.count;

                    for (var i = 0; i < vm.revieweds.length; i++) {
                        vm.revieweds[i].startDate = vm.revieweds[i].startDate.substr(0,12).trim();
                        vm.revieweds[i].endDate = vm.revieweds[i].endDate.substr(0,12).trim();
                    }

                    if (!vm.viewPending) {
                        setPagination(vm.revieweds_count);
                    }
                }
            });
        }

        function handleApplications(action) {
            var toSend = {"pendings": []};
            for (var i = 0; i < vm.checkedList.length; i++) {
                if (vm.checkedList[i].value) {
                    vm.checkedList[i].status = action;
                    toSend.pendings.push(vm.checkedList[i]);
                }
            }

            return applicationservice.managePendings(toSend).then(function(data) {
                if (data === false) {
                    logger.error("Unable to approve/reject applications!");
                }
                activate();
            });
        }

        function updateCheckedList(value, key, isMaster) {
            if (isMaster) {
                for (var i = 0; i < vm.checkedList.length; i++) {
                    vm.checkedList[i].value = value;
                    vm.boxes[vm.checkedList[i].id] = value;
                }

                vm.checkedCounter = value ? vm.checkedList.length : 0;

                return;
            }

            for (var i = 0; i < vm.checkedList.length; i++) {
                if (vm.checkedList[i].id == key) {
                    vm.checkedList[i].value = value;

                    vm.checkedCounter += value ? 1 : -1;

                    return;
                }
            }
        }

        function toggleView(tab) {
            if (tab == 'pending-application') {
                vm.viewPending =  true;
                setPagination(vm.pendings_count);
            } else {
                vm.viewPending = false;
                setPagination(vm.revieweds_count);
            }
        }

        /*
         Sets the pagination and its parameters when a search is used
         Makes sure that clicking a page will return the correct results
         */
        function setPagination(count) {
            vm.lastPage = (count % 30 == 0) ? count/30 : Math.floor(count/30) + 1;
            vm.paginationList = [];
            for (var i=1; i<=vm.lastPage; i++) {
                vm.paginationList.push(i);
            }
        }

        function getNextProfilePage() {
            if (vm.currentPage < vm.lastPage) {
                getPage(vm.currentPage + 1);
            }
        }

        function getPreviousProfilePage() {
            if (vm.currentPage > 1) {
                getPage(vm.currentPage - 1);
            }
        }

        function getPage(i) {
            if (i == vm.currentPage) {
                console.log(vm.currentPage);
            } else if (i > 0 && i <= vm.lastPage) {
                vm.currentPage = i;
                loadResources(i);
            } else {
                logger.error("[Error] Tried to load invalid page: " + i);
            }
        }

        function loadResources(i) {
            var promises = [getPendings(i, 'date', vm.query, vm.types), getRevieweds(i, 'date', vm.query, vm.types)];
            return $q.all(promises).then(function() {});
        }
    }
})();
