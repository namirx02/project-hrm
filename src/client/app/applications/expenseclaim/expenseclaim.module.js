(function() {
    'use strict';

	/* 'app.todolist' -> 'app.leaveapplication' */
    angular.module('app.expenseclaim', [
        'app.core',
        'app.widgets'
      ]);
})();
