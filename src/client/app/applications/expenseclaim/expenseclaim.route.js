(function() {
    'use strict';

	/* 'app.Leaveapplication' -> 'app.expenseclaim' */
    angular
        .module('app.expenseclaim')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Expenseclaim',
                config: {
                    url: '/applications/claims',
                    templateUrl: 'app/applications/expenseclaim/expenseclaim.html',
                    controller: 'ExpenseclaimController',
                    controllerAs: 'vm',
                    title: 'Expenseclaim',
                    settings: {
                        nav: 2,
                        parent: 'Application',
                        // content: '<i class="fa fa-credit-card"></i> <span>Expense Claim</span>'
                        name: "EXPENSE_CLAIM",
                        className: "fa fa-credit-card"
                    }
                }
            }
        ];
    }
})();
