/* jshint -W117, -W030 */
/* 'todolist routes' -> 'expenseclaim routes' */
describe('expenseclaim routes', function () {
    describe('state', function () {
		/* 'app/todolist/todolist.html' -> 'app/application/expenseclaim/leave_application.html' */
        var view = 'app/application/expenseclaim/expenseclaim.html';

		/* module('app.todolist', bard.fakeToastr) -> module('app.expenseclaim', bard.fakeToastr); */
        beforeEach(function() {
            module('app.expenseclaim', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

		/* 'todolist' -> 'expenseclaim' */
        it('should map state expenseclaim to url / ', function() {
            expect($state.href('expenseclaim', {})).to.equal('/');
        });

        it('should map /expenseclaim route to planner View template', function () {
            expect($state.get('expenseclaim').templateUrl).to.equal(view);
        });

        it('of application should work with $state.go', function () {
            $state.go('expenseclaim');
            $rootScope.$apply();
            expect($state.is('expenseclaim'));
        });
    });
});
