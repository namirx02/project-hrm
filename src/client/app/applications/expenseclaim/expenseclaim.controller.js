	(function () {
    'use strict';

	/* 'app.leaveapplication' -> 'app.expenseclaim
	'LeaveapplicationController' -> ExpenseclaimController*/
    angular
        .module('app.expenseclaim')
        .controller('ExpenseclaimController', ExpenseclaimController);

    ExpenseclaimController.$inject = ['$q', 'applicationservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function ExpenseclaimController($q, applicationservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

		/* 'Leave Application' -> 'Expense Claim' */
        vm.title = 'Expense Claim';

        /* 'vm.submitleave' -> 'vm.submitClaim' */
        vm.expense = {};
        vm.submitClaim = submitClaim;

        activate();

        function activate() {
            var promises = [init()];
            return $q.all(promises).then(function() {
                logger.info('Activated Expense Claim View');
            });
        }

        function init() {
            vm.expense = {};
        }

        function submitClaim(expense) {
            var isEmptyField = false;
            var errorMessage = '';

            if(!$('#fromDate').data("DateTimePicker").date()){
                errorMessage += '\"Start date of trip\" ';
                isEmptyField = true;
            }
            if(!$('#toDate').data("DateTimePicker").date()){
                errorMessage += '\"End date of trip\" ';
                isEmptyField = true;
            }


            if (!isEmptyField) {
                expense.startDate = $('#fromDate').data("DateTimePicker").date().format('YYYY-MM-DD');
                expense.endDate = $('#toDate').data("DateTimePicker").date().format('YYYY-MM-DD');
                return applicationservice.applyExpense({"Expense": expense}).then(function(data) {
                    if (data === false)
                        logger.error('Unable to submit expense claim!');
                    else {
                        init();
                        logger.success('Expense claim successfully submitted!')
                    }
                });
            } else{
                logger.error(errorMessage + " field can\'t be left empty!");
                return;
            }

        }
    }
})();
