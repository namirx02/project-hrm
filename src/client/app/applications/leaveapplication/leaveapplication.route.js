(function() {
    'use strict';

	/* 'app.todolist -> 'app.Leaveapplication' */
    angular
        .module('app.leaveapplication')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Leaveapplication',
                config: {
                    url: '/applications/leave',
                    templateUrl: 'app/applications/leaveapplication/leave_application.html',
                    controller: 'LeaveapplicationController',
                    controllerAs: 'vm',
                    title: 'Leaveapplication',
                    settings: {
                        nav: 2,
                        parent: 'Application',
                        // content: '<i class="leave-application-logo"></i> <span>Leave Application</span>'
                        name: "Leave Application",
                        className: "leave-application-logo"
                    }
                }
            }
        ];
    }
})();
