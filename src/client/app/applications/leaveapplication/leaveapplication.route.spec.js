/* jshint -W117, -W030 */
/* 'todolist routes' -> 'leaveapplication routes' */
describe('leaveapplication routes', function () {
    describe('state', function () {
		/* 'app/todolist/todolist.html' -> 'app/application/leaveapplication/leave_application.html' */
        var view = 'app/application/leaveapplication/leave_application.html';

		/* module('app.todolist', bard.fakeToastr) -> module('app.leaveapplication', bard.fakeToastr); */
        beforeEach(function() {
            module('app.leaveapplication', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

		/* 'todolist' -> 'leaveapplication' */
        it('should map state leaveapplication to url / ', function() {
            expect($state.href('leaveapplication', {})).to.equal('/');
        });

        it('should map /leaveapplication route to planner View template', function () {
            expect($state.get('leaveapplication').templateUrl).to.equal(view);
        });

        it('of application should work with $state.go', function () {
            $state.go('leaveapplication');
            $rootScope.$apply();
            expect($state.is('leaveapplication'));
        });
    });
});
