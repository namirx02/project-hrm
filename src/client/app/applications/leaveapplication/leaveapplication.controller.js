	(function () {
    'use strict';

	/* 'app.todolist' -> 'app.leaveapplication'
	'TodolistController' -> ' 'LeaveapplicationController' */
    angular
        .module('app.leaveapplication')
        .controller('LeaveapplicationController', LeaveapplicationController);

    LeaveapplicationController.$inject = ['$q', 'applicationservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function LeaveapplicationController($q, applicationservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

		/* 'To-do List' -> 'Leave Application' */
        vm.title = 'Leave Application';

        vm.leave = {};
        vm.submitLeave = submitLeave;

        activate();

        function activate() {
            var promises = [init()];
            return $q.all(promises).then(function() {
                logger.info('Activated Leave Application View');
            });
        }

        function init() {
            setTimeout(
                function loadScript() {
                    $(function() {
                        $('#leave_application_date_from').datetimepicker({
                            format: 'DD/MM/YYYY',
                        });

                        $('#leave_application_date_to').datetimepicker({
                            format: 'DD/MM/YYYY'
                        });
                    });
                }
            );
        }

        function submitLeave() {
            var isEmptyField = false;
            var errorMessage = '';

            if(!$('#leave_application_date_from').data("DateTimePicker").date()){
                errorMessage += '\"Start date of leave\" ';
                isEmptyField = true;
            }
            if(!$('#leave_application_date_to').data("DateTimePicker").date()){
                errorMessage += '\"End date of leave\" ';
                isEmptyField = true;
            }
            if(!vm.leave.type){
                errorMessage += '\"Type of leave\" ';
                isEmptyField = true;
            }

            if (!isEmptyField) {
                vm.leave.startDate = $('#leave_application_date_from').data("DateTimePicker").date().format('YYYY-MM-DD');
                vm.leave.endDate = $('#leave_application_date_to').data("DateTimePicker").date().format('YYYY-MM-DD');
                return applicationservice.applyLeave({"Leave": vm.leave}).then(function(data) {
                    if (data === false)
                        logger.error("Unable to submit leave request!");
                    else {
                        vm.leave = {};
                        logger.success('Leave request submitted!')
                    }
                });
            } else{
                logger.error(errorMessage + " field can\'t be left empty!");
                return;
            }

        }
    }
})();
