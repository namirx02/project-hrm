/* jshint -W117, -W030 */
/* 'TodolistController' -> 'LeaveapplicationController' */
describe('LeaveapplicationController', function() {
    var controller;
    var people = mockData.getMockPeople();

	/* 'app.profile' -> 'app.leaveapplication' */
    beforeEach(function() {
        bard.appModule('app.leaveapplication');
        bard.inject('$controller', '$log', '$q', '$rootScope', 'dataservice');
    });

	/* 'getPeople' may need to be modified */
    beforeEach(function () {
        sinon.stub(dataservice, 'getPeople').returns($q.when(people));
        controller = $controller('LeaveapplicationController');
        $rootScope.$apply();
    });

    bard.verifyNoOutstandingHttpRequests();

    describe('Leave Application controller', function() {
        it('should be created successfully', function () {
            expect(controller).to.be.defined;
        });

        describe('after activate', function() {
            it('should have title of Leave Application', function () {
                expect(controller.title).to.equal('Leaveapplication');
            });

            it('should have logged "Activated"', function() {
                expect($log.info.logs).to.match(/Activated/);
            });

            it('should have news', function () {
                expect(controller.news).to.not.be.empty;
            });

            it('should have at least 1 person', function () {
                expect(controller.people).to.have.length.above(0);
            });

            it('should have people count of 5', function () {
                expect(controller.people).to.have.length(7);
            });
        });
    });
});
