(function() {
    'use strict';

	/* 'app.todolist' -> 'app.leaveapplication' */
    angular.module('app.leaveapplication', [
        'app.core',
        'app.widgets'
      ]);
})();
