	(function () {
    'use strict';

    angular
        .module('app.planner')
        .controller('PlannerController', PlannerController);

    PlannerController.$inject = ['$q', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function PlannerController($q, dataservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'Planner';

        activate();

        function activate() {
            var promises = [];
            return $q.all(promises).then(function() {
                logger.info('Activated Planner View');
            });
        }
    }
})();
