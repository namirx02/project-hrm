(function() {
    'use strict';

    angular
        .module('app.planner')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Planner',
                config: {
                    url: '/',
                    templateUrl: 'app/planner/planner.html',
                    controller: 'PlannerController',
                    controllerAs: 'vm',
                    title: 'Planner',
                    hasChildren: true,
                    settings: {
                        nav: 1,
                        // content: '<i class="planner-logo"></i> <span>Planner</span>'
                        name: "PLANNER",
                        className: "planner-logo"
                    }
                }
            }
        ];
    }
})();
