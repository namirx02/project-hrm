/* jshint -W117, -W030 */
describe('planner routes', function () {
    describe('state', function () {
        var view = 'app/planner/planner.html';

        beforeEach(function() {
            module('app.planner', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state planner to url / ', function() {
            expect($state.href('planner', {})).to.equal('/');
        });

        it('should map /planner route to planner View template', function () {
            expect($state.get('planner').templateUrl).to.equal(view);
        });

        it('of planner should work with $state.go', function () {
            $state.go('planner');
            $rootScope.$apply();
            expect($state.is('planner'));
        });
    });
});
