(function () {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.widgets',
        'app.login',
        'app.dashboard',
        'app.layout',
        'app.profile',
        'app.planner',
        'app.application',
        'app.staff',
        'app.admin',
        'app.settings',
        'app.todolist',
        'app.createaccount',
        'app.organisation',
        'app.news',
        'app.employeebenefit',
        'app.globalsettings',
        'app.calendar',
        'app.applicationhistory',
        'app.overtimeapplication',
        'app.leaveapplication',
        'app.expenseclaim',
        'app.applicationreview',
        'app.logout'
    ]);

})();
