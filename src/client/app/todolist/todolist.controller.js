	(function () {
    'use strict';

    angular
        .module('app.todolist')
        .controller('TodolistController', TodolistController);

    TodolistController.$inject = ['$q', 'todolistservice', 'helperservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function TodolistController($q, todolistservice, helperservice, dataservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'To-do List';
        vm.isEditing = false;

        vm.addItem = addItem;
        vm.updateItem = updateItem;
        vm.deleteItem = deleteItem;
        vm.toggleDone = toggleDone;
        vm.popupModal = popupModal;
        vm.markDone = markDone;
        vm.markUndone = markUndone;
        vm.startEdit = startEdit;
        vm.endEdit = endEdit;

        activate();

        $rootScope.$on("InitTodo", function(event,args){
            activate();
        });

        function activate() {
            var promises = [init(), getItems(), dataservice.resizePage()];

            return $q.all(promises).then(function() {
                logger.info('Activated To-do List View');
            });
        }

        function init() {
            vm.offset = (new Date().getTimezoneOffset()/60)*(-1);
            vm.showingDone = false;
            vm.items = [];
            vm.doneItems = [];
            vm.todoModal = {};
            vm.oldModalDone = null;
        }

        function startEdit() {
            vm.isEditing = true;
        }

        function endEdit() {
            vm.isEditing = false;
        }

        function popupModal(item) {
            vm.todoModal = item;
            vm.oldModalDone = vm.todoModal.done;

            $('#DueDate').datetimepicker({
                format: 'YYYY-MM-DD hh:mm a'
            });

            if (vm.todoModal.timeDue) {
                vm.todoModal.timeDue = new Date(vm.todoModal.timeDue);
                $('#DueDate').attr('value', moment(vm.todoModal.timeDue).format('YYYY-MM-DD hh:mm a'));
            } else {
                $('#DueDate').attr('value', '');
            }

            $('#todoModal').modal();
        }

        function toggleDone() {
            vm.showingDone = vm.showingDone ? false : true;
        }

        // $rootScope.on('todo-done', function(event, data) {
        //     var id = data.id;
        //
        //     vm.items.
        // });

        function markDone(item) {
            item.done = true;
            // vm.items.splice(vm.items.indexOf(item), 1);
            // vm.doneItems.push(item);
            updateItem(item, true, vm.items, vm.doneItems, null);
        }

        function markUndone(item) {
            item.done = false;
            // vm.doneItems.splice(vm.doneItems.indexOf(item), 1);
            // vm.items.push(item);
            updateItem(item, true, vm.doneItems, vm.items, null);
        }

        function addItem(newItem) {
            newItem.done = false;

            return todolistservice.addItem(newItem).then(function (data) {
                if (data === false) {
                    logger.error('Unable to save todo item!');
                } else {
                    newItem.id = data;
                    vm.items.push(newItem);
                    logger.success('Todo item successfully saved!');
                }
                vm.newItem = {};
            });
        }

        function updateItem(item, moveItem, listFrom, listTo, oldDone) {
            if(!moveItem && $('#DueDate').data("DateTimePicker").date()){
                item.timeDue = $('#DueDate').data("DateTimePicker").date().format('YYYY-MM-DDTHH:mm:00+'+vm.offset);
            }

            if (oldDone != null) {
                if (oldDone != item.done) {
                    moveItem = true;
                    listFrom = oldDone ? vm.doneItems : vm.items;
                    listTo = oldDone ? vm.items : vm.doneItems;
                }
            }

            return todolistservice.updateItem(item).then(function (data) {
                if (data === false) {
                    logger.error('Unable to save todo item!');
                } else {
                    if (moveItem) {
                        listFrom.splice(listFrom.indexOf(item), 1);
                        listTo.push(item);
                    }
                    vm.isEditing = false;
                    logger.success('Todo item successfully saved!');
                    $rootScope.$broadcast('NewTodoTask');
                }
            });
        }

        function deleteItem(item, list) {
            return todolistservice.deleteItem(item.id).then(function (data) {
                if (data == false) {
                    logger.error('Unable to delete todo item!');
                } else {
                    list.splice(list.indexOf(item), 1);
                    logger.success('Todo item successfully deleted!');
                }
            });
        }

        function getItems() {
            return todolistservice.getItems().then(function (data) {
              console.log(data);
                if (data === false) {
                    $state.transitionTo('dashboard');
                } else {
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].timeDue) {

                            var tempDue = new Date(data[i].timeDue);
                            data[i].timeDue = new Date(tempDue.getTime() + vm.offset * 60 * 60 * 1000);
                        }

                        if (data[i].done === true) {
                          vm.doneItems.push(data[i]);
                        } else {
                          vm.items.push(data[i]);
                        }
                    }
                }
            })
        }
    }
})();
