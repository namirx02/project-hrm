/* jshint -W117, -W030 */
describe('todolist routes', function () {
    describe('state', function () {
        var view = 'app/todolist/todolist.html';

        beforeEach(function() {
            module('app.todolist', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state todolist to url / ', function() {
            expect($state.href('todolist', {})).to.equal('/');
        });

        it('should map /todolist route to planner View template', function () {
            expect($state.get('todolist').templateUrl).to.equal(view);
        });

        it('of planner should work with $state.go', function () {
            $state.go('todolist');
            $rootScope.$apply();
            expect($state.is('todolist'));
        });
    });
});
