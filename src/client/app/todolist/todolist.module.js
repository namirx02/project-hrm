(function() {
    'use strict';

    angular.module('app.todolist', [
        'app.core',
        'app.widgets'
      ]);
})();
