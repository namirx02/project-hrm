(function() {
    'use strict';

    angular
        .module('app.todolist')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Todolist',
                config: {
                    url: '/to-do-list',
                    templateUrl: 'app/todolist/todolist.html',
                    controller: 'TodolistController',
                    controllerAs: 'vm',
                    title: 'Todolist',
                    settings: {
                        nav: 2,
                        parent: 'Planner',
                        // content: '<i class="to-do-list-logo"></i> <span>To-do List</span>'
                        name: "TODO_LIST",
                        className: "to-do-list-logo"
                    }
                }
            }
        ];
    }
})();
