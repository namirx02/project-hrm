(function() {
    'use strict';

    angular
        .module('app.calendar')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Calendar',
                config: {
                    url: '/calendar',
                    templateUrl: 'app/calendar/calendar.html',
                    controller: 'CalendarController',
                    controllerAs: 'vm',
                    title: 'Calendar',
                    settings: {
                        nav: 2,
                        parent: 'Planner',
                        // content: '<i class="calendar-logo"></i> <span>Calendar</span>'
                        name: "CALENDAR",
                        className: "calendar-logo"
                    }
                }
            }
        ];
    }
})();
