	(function () {
    'use strict';

    angular
        .module('app.calendar', ['ui.calendar', 'ui.bootstrap', 'ngTagsInput'])
        .controller('CalendarController', CalendarController);

    CalendarController.$inject = ['$q', '$scope', '$compile', 'helperservice', 'profileservice', 'calendarservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope', 'uiCalendarConfig'];
    /* @ngInject */
    function CalendarController($q, $scope, $compile, helperservice, profileservice, calendarservice, dataservice, logger, $state, $filter, $window, $rootScope, uiCalendarConfig) {
        var vm = this;

        vm.title = 'Calendar';

        vm.isEditing = false;
        vm.isNew = false;
        vm.changeActive = 'month';

        vm.saveEvent = saveEvent;
        vm.startEdit = startEdit;
        vm.endEdit = endEdit;
        vm.deleteEvent = deleteEvent;
        vm.queryTags = queryTags;

        activate();

        function activate() {
            var currentUser = sessionStorage.getItem('user');
            if (currentUser) {
                // var today = new Date();
                // var month = today.getMonth();
                // var year = today.getYear();
                vm.currentUser = JSON.parse(currentUser);
                var promises = [setup(),init()];
            } else {
                $state.transitionTo('dashboard');
            }

            return $q.all(promises).then(function() {
                logger.info('Activated Calendar View');
            });
        }

        function queryTags(query) {
            return profileservice.getProfilesByString({page: 1, string: query}).then(function(data) {
                var filteredNames = data === false ? [] : data;

                for (var i = 0; i < filteredNames.length; i++) {
                    filteredNames[i].name += " (" + filteredNames[i].username + ")";
                }
                return $q.when(filteredNames);
            })
        }

        function init() {
            vm.offset = (new Date().getTimezoneOffset()/60)*(-1);
            vm.isEditing = true;
            vm.isNew = true;
            vm.modalEvent = {header: "New event", title: "", type: "", start: "", end: ""};
            vm.profiles = [];
        }

        function setup() {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            vm.changeTo = 'Hungarian';
            /* event source that pulls from google.com */
            vm.eventSource = {
                className: 'gcal-event',           // an option!
                currentTimezone: 'Australia/Sydney' // an option!
            };

            vm.events = [];

            /* event source that calls a function on every view switch */
            vm.eventsF = function (start, end, timezone, callback) {
                var s = new Date(start).getTime() / 1000;
                var e = new Date(end).getTime() / 1000;
                var m = new Date(start).getMonth();
                var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
                callback(events);
            };

            vm.calEventsExt = {
                color: '#f00',
                textColor: 'yellow',
                events: [
                    {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: true},
                    {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
                    {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
                ]
            };
            /* alert on eventClick */
            vm.alertOnEventClick = function( date, jsEvent, view){
                init();
                vm.modalEvent = vm.events.filter(function(item) {
                   return item.id == date.id;
                })[0];

                vm.modalEvent.start = new Date(vm.modalEvent.start);
                vm.modalEvent.end = new Date(vm.modalEvent.end);
                vm.modalEvent.header = "Edit event";

                vm.profiles = date.Profiles;
                // vm.profiles = Object(date.Profiles.filter(function(n) {
                //     if(n.staffNum != vm.currentUser.staffNum){
                //         return true;
                //     }
                // }));

                vm.isNew = false;
                vm.isEditing = false;

                var endStr = "EndDate";
                var startStr = "StartDate";

                $('#' + endStr).datetimepicker({
                    format: 'YYYY-MM-DD hh:mm a'
                    // defaultDate: vm.modalEvent.end
                });
                $('#' + endStr).attr('value', moment(vm.modalEvent.end).format('YYYY-MM-DD hh:mm a'));
                $('#' + startStr).datetimepicker({
                    format: 'YYYY-MM-DD hh:mm a'
                    // defaultDate: vm.modalEvent.start
                });
                $('#' + startStr).attr('value', moment(vm.modalEvent.start).format('YYYY-MM-DD hh:mm a'));
                $('#calendarModal').modal();
            };
            /* alert on Drop */
            vm.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
                var start = new Date(new Date(event.start).getTime() - vm.offset * 60 * 60 * 1000);
                var end = new Date(new Date(event.end).getTime() - vm.offset * 60 * 60 * 1000);
                var id = event.id;

                var fstart = formatDate(start,'YYYY-MM-DDTHH:mm:00+'+vm.offset);
                var fend = formatDate(end,'YYYY-MM-DDTHH:mm:00+'+vm.offset);
                var currentEvent = findItem(vm.events, id, 'id');

                if (currentEvent && currentEvent.length > 0) {
                    var objTosent = formatObjToSave(fstart,fend,currentEvent[0],currentEvent[0].Profiles);
                    return saveEventRequest(objTosent);
                }
            };
            /* alert on Resize */
            vm.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
                var start = new Date(new Date(event.start).getTime() - vm.offset * 60 * 60 * 1000);
                var end = new Date(new Date(event.end).getTime() - vm.offset * 60 * 60 * 1000);
                var id = event.id;

                var fstart = formatDate(start,'YYYY-MM-DDTHH:mm:00+'+vm.offset);
                var fend = formatDate(end,'YYYY-MM-DDTHH:mm:00+'+vm.offset);
                var currentEvent = findItem(vm.events, id, 'id');

                if (currentEvent && currentEvent.length > 0) {
                    var objTosent = formatObjToSave(fstart,fend,currentEvent[0],currentEvent[0].Profiles);
                    return saveEventRequest(objTosent);
                }
            };
            /* add and removes an event source of choice */
            vm.addRemoveEventSource = function(sources,source) {
                var canAdd = 0;
                angular.forEach(sources,function(value, key){
                    if(sources[key] === source){
                        sources.splice(key,1);
                        canAdd = 1;
                    }
                });
                if(canAdd === 0){
                    sources.push(source);
                }
            };
            /* add custom event*/
            vm.addEvent = function() {
                init();
                vm.isNew = true;
                vm.isEditing = true;
                vm.modalEvent = {header: "New event", title: "", type: "", start: "", end: ""};
                $('#EndDate').datetimepicker({
                    format: 'YYYY-MM-DD hh:mm a'
                });
                $('#EndDate').attr('value', moment(new Date()).format('YYYY-MM-DD hh:mm a'));
                $('#StartDate').datetimepicker({
                    format: 'YYYY-MM-DD hh:mm a'
                });
                $('#EndDate').attr('value', moment(new Date()).format('YYYY-MM-DD hh:mm a'));
                $('#calendarModal').modal();
            };
            /* Change View */
            vm.changeView = function(view,calendar) {
                vm.changeActive = view;
                uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
                // $('#calendar-holder').fullCalendar('changeView', view);
            };
            /* Change View */
            vm.renderCalender = function(calendar) {
                // $('#calendar-holder').fullCalendar('render');
                if(uiCalendarConfig.calendars[calendar]){
                    uiCalendarConfig.calendars[calendar].fullCalendar('render');
                }

                var cal = $('#calendar-holder').fullCalendar('getCalendar');
                // var start = new Date(cal.view.start._d);
                // var end = new Date(cal.view.end._d);
                //
                // var fstart = moment(start).format('YYYY-MM-DDT00:00:00');
                // var fend = moment(end).format('YYYY-MM-DDT00:00:00');

                var fstart = formatDate(cal.view.start._d,'YYYY-MM-DDT00:00:00');
                var fend = formatDate(cal.view.end._d,'YYYY-MM-DDT00:00:00');
                getEvents(vm.offset, fstart, fend, vm.currentUser.staffNum);
            };
            /* Render Tooltip */
            vm.eventRender = function( event, element, view ) {
                element.attr({'tooltip': event.title,
                    'tooltip-append-to-body': true});
                $compile(element)(vm);
            };
            /* config object */
            $scope.uiConfig = {
                calendar:{
                    height: 450,
                    editable: true,
                    header:{
                        left: 'title',
                        center: '',
                        right: 'today prev,next'
                    },
                    eventClick: vm.alertOnEventClick,
                    eventDrop: vm.alertOnDrop,
                    eventResize: vm.alertOnResize,
                    eventRender: vm.eventRender,
                    viewRender: vm.renderCalender
                }
            };

            vm.changeLang = function() {
                if(vm.changeTo === 'Hungarian'){
                    vm.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
                    vm.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
                    vm.changeTo= 'English';
                } else {
                    vm.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    vm.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                    vm.changeTo = 'Hungarian';
                }
            };
            /* event sources array*/
            vm.eventSources = [vm.events, vm.eventSource, vm.eventsF];
        }

        function getEvents(offset, from, to, owner) {
            return calendarservice.getUserEvents(offset, from, to, owner).then(function(data) {
                vm.events.length = 0;
                var len = 0;
                if (data && data.length) {
                    len = data.length;
                }
                for (var i = 0; i < len; i++) {
                    var tempStart = new Date(data[i].start);
                    data[i].start = new Date(tempStart.getTime() + vm.offset * 60 * 60 * 1000);
                    var tempEnd = new Date(data[i].end);
                    data[i].end = new Date(tempEnd.getTime() + vm.offset * 60 * 60 * 1000);
                    // data[i].color = 'red';
                    // data[i].textColor = 'yellow';
                }
                vm.events.push.apply(vm.events, data);
            });
        }

        function saveEvent(modalEvent, profiles) {
            var isEmptyField = false;
            var errorMessage = '';

            if(modalEvent){     //check if profile object exists or address object exists or not (It does not exist if all of the fields of an object is empty)
                // check for empty field
                // --------- Personal Details ---------
                if(!modalEvent.title){
                    errorMessage += '\"Title\" ';
                    isEmptyField = true;
                }

                if(!$('#StartDate').data("DateTimePicker").date()){
                    errorMessage += '\"Start time\" ';
                    isEmptyField = true;
                }

                if(!$('#EndDate').data("DateTimePicker").date()){
                    errorMessage += '\"End time\" ';
                    isEmptyField = true;
                }

                if(!modalEvent.type){
                    modalEvent.type = 'general';
                } else {
                    if (modalEvent.type == 'important') {
                        modalEvent.color = 'red';
                        modalEvent.textColor = 'yellow';
                    } else if (modalEvent.type == 'celebration') {
                        modalEvent.color = 'green';
                        modalEvent.textColor = '';
                    }
                }

                if (!modalEvent.alert) {
                    modalEvent.alert = '5min';
                }

                if(isEmptyField === false){    //is any important field left empty?
                    var start = $('#StartDate').data("DateTimePicker").date().format('YYYY-MM-DDTHH:mm:00+'+vm.offset);
                    var end = $('#EndDate').data("DateTimePicker").date().format('YYYY-MM-DDTHH:mm:00+'+vm.offset);

                    var objTosent = formatObjToSave(start,end,modalEvent,profiles);

                    return saveEventRequest(objTosent);
                }else{
                    logger.error(errorMessage + " field can\'t be left empty!");
                    return;
                }
            }else{
                logger.error("Please enter event detail!");
            }
        }

        function saveEventRequest(objTosent) {
            return calendarservice.addEvent(objTosent).then(function(data) {
                if (data && data.Status == "Success") {
                    if (vm.isOwnerInEvent)
                        if (!objTosent.Event.hasOwnProperty("id")) {
                            objTosent.Event.Profiles = objTosent.Profiles;
                            objTosent.Event.id = parseInt(data.NewID);
                            objTosent.Event.start = convertStringToDate(objTosent.Event.start);
                            objTosent.Event.end = convertStringToDate(objTosent.Event.end);
                            vm.events.push(objTosent.Event);
                        }
                        else {
                            for (var i = 0; i < vm.events.length; i++) {
                                if (vm.events[i].id == objTosent.Event.id) {
                                    objTosent.Event.Profiles = objTosent.Profiles;

                                    objTosent.Event.start = convertStringToDate(objTosent.Event.start);
                                    objTosent.Event.end = convertStringToDate(objTosent.Event.end);

                                    vm.events.splice(i, 1);
                                    vm.events.push(objTosent.Event);
                                    break;
                                }
                            }
                        }

                    vm.renderCalender($scope.uiConfig.calendar);
                    logger.success("Event saved!");
                    $('#calendarModal').modal('hide');
                    $rootScope.$broadcast('NewCalendarEvent');
                }
                else {
                    logger.error("Unable to save event!");
                }

                $('#modal').modal('toggle');
            });
        }

        function startEdit() {
            vm.isEditing = true;
        }

        function endEdit() {
            vm.isEditing = false;
        }

        function deleteEvent(modalEvent, profiles) {
            return calendarservice.deleteEvent(modalEvent.id).then(function(data) {
                if (data == "Success") {
                    var ownerInEvent = profiles.filter(function(item) {
                       return item.staffNum == vm.currentUser.staffNum;
                    });

                    if (ownerInEvent.length > 0) {
                        for (var i = 0; i < vm.events.length; i++) {
                            if (vm.events[i].id == modalEvent.id) {
                                vm.events.splice(i, 1);
                                init();
                                logger.success("Event deleted!");
                            }
                        }
                    }
                } else {
                    logger.error("Unable to delete event!");
                }
            });
        }

        function findItem(array, id, attr) {
            return array.filter(function(item) {
                return item[attr] == id;
            })
        }

        function formatDate(strDate, strFormat) {
            var fdate = calendarservice.formatDate(strDate, strFormat);
            return fdate;
        }

        function formatObjToSave(start, end, event, profiles) {
            var objTosent = {Event: event, Profiles: []};
            objTosent.Event.start = start;
            objTosent.Event.end = end;

            vm.isOwnerInEvent = false;
            for (var i = 0; i < profiles.length; i++) {
                objTosent.Profiles.push({
                    "staffNum": profiles[i].staffNum,
                    "name": profiles[i].name,
                    "email": profiles[i].email
                });
                if (vm.currentUser.staffNum == profiles[i].staffNum)
                    vm.isOwnerInEvent = true;
            }

            if (objTosent.Event.hasOwnProperty("Profiles")) {
                delete objTosent.Event.Profiles;
            }

            return objTosent;
        }

        function convertStringToDate(str) {
            return helperservice.convertStringToDate(str);
        }
    }
})();
