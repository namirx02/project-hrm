(function() {
    'use strict';

    angular
        .module('app.profile')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'profile',
                config: {
                    url: '/profile/:id',
                    templateUrl: 'app/profile/profile.html',
                    controller: 'ProfileController',
                    controllerAs: 'vm',
                    title: 'profile',
                    hideFromSidebar: true,
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-user"></i> <span>Profile</span>'
                    }
                }
            }
        ];
    }
})();
