	(function() {
	    'use strict';

	    angular
	        .module('app.profile')
	        .filter('dateInMillis', function() { //inline date filters
	            return function(dateString) {
	                return Date.parse(dateString);
	            };
	        })
	        .controller('ProfileController', ProfileController);

	    ProfileController.$inject = ['$q', 'profileservice', 'logger', '$state', '$filter', '$window', '$rootScope', 'config','countryservice'];
	    /* @ngInject */
	    function ProfileController($q, profileservice, logger, $state, $filter, $window, $rootScope, config, countryservice) {
	        var vm = this;

          vm.countries = [];
	        vm.title = 'Profile';
	        vm.viewInfo = 1;
	        vm.toggleView = toggleView;
	        vm.updateProfile = updateProfile;
	        vm.imageURL = "http://i.imgur.com/G8rcrgj.jpg";
	        vm.popupContract = popupContract;
	        vm.uploadContract = uploadContract;

	        activate();

	        function activate() {

	            var promises = [findProfile(), initialiseChart(), init()];
	            return $q.all(promises).then(function() {
	                logger.info('Activated Profile View');
	            });
	        }

          function init(){
              vm.countries = countryservice.countries();
          }

	        function findProfile() {
	            var id = $state.params.id;

	            if (!id) {
	                logger.error('Error handling request!');
	            } else {
	                return profileservice.getProfileInfo(id).then(function(data) {

	                    if (typeof data != 'object') {
	                        $state.transitionTo('login');
	                    }

	                    vm.profile = data;
	                    vm.imageURL = vm.profile.imageURL ? config.host + "/" + vm.profile.imageURL : vm.imageURL;
	                    vm.contractURL = vm.profile.contractURL ? config.host + "/" + vm.profile.contractURL : "";
	                });
	            }
	        }

	        function initialiseChart() {
	            //------------ Work Hour Chart -----------------------
	            // Get context with jQuery - using jQuery's .get() method.
	            var workChartCanvas = $("#workHourChart").get(0).getContext("2d");
	            // This will get the first returned node in the jQuery collection.
	            var workChart = new Chart(workChartCanvas);

	            var workChartData = {
	                labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
	                datasets: [{
	                        label: "Department Average",
	                        fillColor: "rgb(210, 214, 222)",
	                        strokeColor: "rgb(210, 214, 222)",
	                        pointColor: "rgb(210, 214, 222)",
	                        pointStrokeColor: "#c1c7d1",
	                        pointHighlightFill: "#fff",
	                        pointHighlightStroke: "rgb(220,220,220)",
	                        data: [65, 59, 80, 81, 56, 55, 40]
	                    },
	                    {
	                        label: "Your Work Hours",
	                        fillColor: "rgba(60,141,188,0.9)",
	                        strokeColor: "rgba(60,141,188,0.8)",
	                        pointColor: "#3b8bba",
	                        pointStrokeColor: "rgba(60,141,188,1)",
	                        pointHighlightFill: "#fff",
	                        pointHighlightStroke: "rgba(60,141,188,1)",
	                        data: [28, 48, 40, 19, 86, 27, 90]
	                    }
	                ]
	            };

	            var workChartOptions = {
	                //Boolean - If we should show the scale at all
	                showScale: true,
	                //Boolean - Whether grid lines are shown across the chart
	                scaleShowGridLines: false,
	                //String - Colour of the grid lines
	                scaleGridLineColor: "rgba(0,0,0,.05)",
	                //Number - Width of the grid lines
	                scaleGridLineWidth: 1,
	                //Boolean - Whether to show horizontal lines (except X axis)
	                scaleShowHorizontalLines: true,
	                //Boolean - Whether to show vertical lines (except Y axis)
	                scaleShowVerticalLines: true,
	                //Boolean - Whether the line is curved between points
	                bezierCurve: true,
	                //Number - Tension of the bezier curve between points
	                bezierCurveTension: 0.3,
	                //Boolean - Whether to show a dot for each point
	                pointDot: false,
	                //Number - Radius of each point dot in pixels
	                pointDotRadius: 4,
	                //Number - Pixel width of point dot stroke
	                pointDotStrokeWidth: 1,
	                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	                pointHitDetectionRadius: 20,
	                //Boolean - Whether to show a stroke for datasets
	                datasetStroke: true,
	                //Number - Pixel width of dataset stroke
	                datasetStrokeWidth: 2,
	                //Boolean - Whether to fill the dataset with a color
	                datasetFill: true,
	                //String - A legend template
	                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
	                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	                maintainAspectRatio: true,
	                //Boolean - whether to make the chart responsive to window resizing
	                responsive: true
	            };

	            //Create the line chart
	            workChart.Line(workChartData, workChartOptions);

	            //------------ Contract signed Chart -----------------------
	            var barChartCanvas = $("#contractChart").get(0).getContext("2d");
	            var barChart = new Chart(barChartCanvas);
	            var barChartData = {
	                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
	                datasets: [{
	                        label: "Department Average",
	                        fillColor: "rgba(210, 214, 222, 1)",
	                        strokeColor: "rgba(210, 214, 222, 1)",
	                        pointColor: "rgba(210, 214, 222, 1)",
	                        pointStrokeColor: "#c1c7d1",
	                        pointHighlightFill: "#fff",
	                        pointHighlightStroke: "rgba(220,220,220,1)",
	                        data: [65, 59, 80, 81, 56, 55, 40, 43, 41, 30, 20, 19]
	                    },
	                    {
	                        label: "Your Performance",
	                        fillColor: "rgba(60,141,188,0.9)",
	                        strokeColor: "rgba(60,141,188,0.8)",
	                        pointColor: "#3b8bba",
	                        pointStrokeColor: "rgba(60,141,188,1)",
	                        pointHighlightFill: "#fff",
	                        pointHighlightStroke: "rgba(60,141,188,1)",
	                        data: [28, 48, 40, 19, 86, 27, 90, 20, 21, 40, 50, 60]
	                    }
	                ]
	            };;
	            barChartData.datasets[1].fillColor = "#3b8bba";
	            barChartData.datasets[1].strokeColor = "#3b8bba";
	            barChartData.datasets[1].pointColor = "#3b8bba";
	            var barChartOptions = {
	                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	                scaleBeginAtZero: true,
	                //Boolean - Whether grid lines are shown across the chart
	                scaleShowGridLines: true,
	                //String - Colour of the grid lines
	                scaleGridLineColor: "rgba(0,0,0,.05)",
	                //Number - Width of the grid lines
	                scaleGridLineWidth: 1,
	                //Boolean - Whether to show horizontal lines (except X axis)
	                scaleShowHorizontalLines: true,
	                //Boolean - Whether to show vertical lines (except Y axis)
	                scaleShowVerticalLines: true,
	                //Boolean - If there is a stroke on each bar
	                barShowStroke: true,
	                //Number - Pixel width of the bar stroke
	                barStrokeWidth: 2,
	                //Number - Spacing between each of the X value sets
	                barValueSpacing: 5,
	                //Number - Spacing between data sets within X values
	                barDatasetSpacing: 1,
	                //String - A legend template
	                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
	                //Boolean - whether to make the chart responsive
	                responsive: true,
	                maintainAspectRatio: true
	            };

	            barChartOptions.datasetFill = false;
	            barChart.Bar(barChartData, barChartOptions);

	        }

	        function toggleView(val) {
	            vm.viewInfo = val;
	        }

	        function updateProfile(id) {
	            if (!document.getElementById(id).readOnly) {
	                var objTosent = {
	                    "Profile": vm.profile,
	                    "department_id": vm.profile.department.id,
	                    "office_id": vm.profile.office.id
	                };

	                return profileservice.updateProfile(objTosent).then(function(data) {
	                    console.log(data);
	                });
	            }
	        }

	        function popupContract(profile) {
	            console.log(profile);
	            vm.modalContract = {
	                contractURL: profile.contractURL,
	                name: profile.name
	            };
	            $('#contractModal').modal();
	        }

	        function uploadContract() {
	            if ($('#Contract').prop('files')[0]) {
	                var contract = $('#Contract').prop('files')[0];
	                var reader = new FileReader();
	                var objToSent = {
	                    Contract: ""
	                };
	                reader.addEventListener("load", function() {
	                    objToSent.Contract = reader.result;
	                    console.log(objToSent);
	                    return profileservice.uploadContract(objToSent).then(function(data) {
	                        console.log(data);
	                        if (data == false) {
	                            logger.error('Unable to upload contract!');
	                        } else {
	                            vm.profile.contractURL = config.host + "/" +data;
	                            vm.contractURL = config.host + "/" + data;
	                            logger.success('Contract uploaded successfully!');
	                        }
	                    });
	                }, false);
	                reader.readAsDataURL(contract);
	            } else {

	            }
	        }
	    }
	})();
