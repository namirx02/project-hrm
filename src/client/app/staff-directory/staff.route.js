(function() {
    'use strict';

    angular
        .module('app.staff')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'staff',
                config: {
                    url: '/staff-directory',
                    templateUrl: 'app/staff-directory/staff.html',
                    controller: 'StaffController',
                    controllerAs: 'vm',
                    title: 'staff',
                    settings: {
                        nav: 1,
                        // content: '<i class="staff-directory-logo"></i> <span>Staff Directory</span>'
                        name: "STAFF_DIRECTORY",
                        className: "staff-directory-logo"
                    }
                }
            }
        ];
    }
})();
