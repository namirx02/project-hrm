	(function () {
    'use strict';

    angular
        .module('app.staff')
        .controller('StaffController', StaffController);

    StaffController.$inject = ['$q', 'profileservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope', 'config'];
    /* @ngInject */
    function StaffController($q, profileservice, dataservice, logger, $state, $filter, $window, $rootScope, config) {
        var vm = this;

        vm.title = 'Staff';
        vm.dummy = null;
        vm.users = null;
		vm.modalProfile = null;

		//variables for the alphabet boxes
		vm.alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]


        vm.query = {
		    keyword: "",
            letter: "",
            office: -1,
            department: -1,
            page: 1
        };

		//variables for the offices/departments
		vm.offices = null;
		vm.departments = null;
		vm.availableDepartments = null;
		vm.selectedOffice = null;

		//variables for searching
		vm.searchTerm = null;

		//variables for the pagination
		vm.currentPage = 1;
		vm.LastPage = 1;
		vm.paginationList = [];
		vm.paginationFormat = "all"; //what criteria the pagination is counting (eg. search by letter, office etc.)
		vm.paginationCriteria = null; //if pagination needs query, this is the criteria to use

		//variables for the position levels
		vm.positionList = ["Employee", "Supervisor", "General Manager", "CEO"];
		vm.currentPosition = ""; //holds chosen position as a string for functions to read


		//variables for the modal

		vm.modalIndex = null; // the index of the current modal

		vm.header = "THIS IS A HEADER";

		vm.picture = null;      /* <--------------- This needs to be implemented later */

		vm.name = "FULL NAME";
		vm.position = "POSITION";
		vm.department = "DEPARTMENT";
		vm.office = "OFFICE";
		vm.staffStatus = "EMPLOYMENT STATUS";

		vm.phone = "PHONE NUMBER";
		vm.email = "EMAIL ADDRESS";

		vm.staffNum = "STAFF NUMBER";
		vm.language = "LANGUAGE";
        vm.searchTerm = "";

		vm.isEnabled = false;

		vm.currentPosition = null;

		vm.userIsAdmin = true;


		//function declarations
        vm.getProfiles = getProfiles;
		vm.getProfilesByLetter = getProfilesByLetter;
		vm.getProfilesByOffice = getProfilesByOffice;
		vm.getProfilesByDepartment = getProfilesByDepartment;
		vm.getProfilesByString = getProfilesByString;


		// New pagination
        vm.getNextProfilePageByQuery = getNextProfilePageByQuery;
        vm.getPreviousProfilePageByQuery = getPreviousProfilePageByQuery;
        vm.getPageByQuery = getPageByQuery;

		vm.getOffices = getOffices;
		vm.getDepartments = getDepartments;


        vm.changeProfile = changeProfile;
		vm.setPosition = setPosition;
        vm.showProfile = showProfile;
		vm.enableProfile = enableProfile;
		vm.disableProfile = disableProfile;
		vm.transitionToProfile = transitionToProfile;

		/*
			Activate() Function
			This is run when the page is loaded
		*/
        activate();
        function activate() {
            // var promises = [init(), getProfiles(1), getOffices(), getDepartments()];
            var promises = [init(), getProfilesByQuery(vm.query), getOffices(), getDepartments()];
            return $q.all(promises).then(function() {
                logger.info('Loaded Staff Directory');
            });
        }

        function init() {
            vm.baseURL = config.host + '/';
        }

        function getProfilesByQuery(query) {
            return profileservice.getAllProfiles(query).then(function(data) {
                if (data == false) {

                } else {
                    vm.users = data.profiles;
                    setPaginationByQuery(data.count);
                }
            });
        }

        function setPaginationByQuery(count) {
            vm.lastPage = (count % 15 == 0) ? count/15 : Math.floor(count/15) + 1;
            vm.paginationList = [];
            for (var i=1; i<=vm.lastPage; i++) {
                vm.paginationList.push(i);
            }
        }

        function getNextProfilePageByQuery() {
            if (vm.currentPage < vm.lastPage) {
                getPagByQuery(vm.currentPage + 1);
            }
        }

        function getPreviousProfilePageByQuery() {
            if (vm.currentPage > 1) {
                getPageByQuery(vm.currentPage - 1);
            }
        }

        function getPageByQuery(i) {
            if (i == vm.currentPage) {
                console.log(vm.currentPage);
            } else if (i > 0 && i <= vm.lastPage) {
                vm.currentPage = i;
                vm.query.page = i;
                loadResources();
            } else {
                logger.error("[Error] Tried to load invalid page: " + i);
            }
        }

        function loadResources() {
            var promises = [getProfilesByQuery(vm.query)];
            return $q.all(promises).then(function() {});
        }


		/*
			Loads a page (i.e. 15 profiles) of user profiles into an array for display
			Makes a logger error if nothing is returned
			Backend can return a string "fail" if it fails
			i is the page number (1 = 1-15, 2 = 16-30, etc.)
			by default, loads first page
			returns false if it fails so other functions can know it failed
		*/
        function getProfiles(i) {
            vm.query = {
                keyword: "",
                letter: "",
                office: -1,
                department: -1,
                page: 1
            };
            loadResources();
        }

		function getProfilesByLetter(i, page) {
            if (vm.query.letter != i) {
                vm.query.letter = i;
                loadResources();
            }
        }


		function getProfilesByOffice(i, name) {
            if (vm.query.office != i) {
                vm.query.office = i;
                vm.query.department = -1;
                loadResources();
                getDepartmentsByOffice(i);
            }
        }

		function getProfilesByDepartment(i) {
            if (vm.query.department != i) {
                vm.query.department = i;
                loadResources();
            }
        }

		function getProfilesByString(i, page) {
            if (i.length >= 1) {
                vm.query.keyword = i;
                loadResources();
            }
        }

		/*
			Loads the database offices into an array for display
			Makes a logger error if nothing is returned
			Backend can return a string "fail" if it fails
		*/
		function getOffices() {
        	return dataservice.getOffices().then(function(data) {

				//if data is null then server has nothing to return
				if (data == null) {
					logger.error("[Database Error] no offices found");
				//if data is "fail" there was a server error
				} else if (data == "fail") {
					logger.error("[Database Error] failed to retrieve offices");
				//otherwise ok
				} else {
					vm.offices = data;
				}
        	});
        }

		/*
			Loads the database departments into an array for display
			Makes a logger error if nothing is returned
			Backend can return a string "fail" if it fails
		*/
		function getDepartments() {
        	return dataservice.getDepartments().then(function(data) {

				//if data is null then server has nothing to return
				if (data == null) {
					logger.error("[Database Error] no departments found");
				//if data is "fail" there was a server error
				} else if (data == "fail") {
					logger.error("[Database Error] failed to retrieve departments");
				//otherwise data is ok
				} else {
					vm.departments = data;
					console.log(vm.departments);
				}
        	});
        }

		/*
			Loads the database departments that are associated with an office
			puts them into an array for display
		*/
		function getDepartmentsByOffice(i) {

			vm.availableDepartments = [];
			for (var j=0; j<vm.departments.length; j++) {
				if (vm.departments[j].office.id == i) {
					vm.availableDepartments.push(vm.departments[j]);
				}
			}
        }

		/*
			Fills the profile modal's content with a given profile
			Separate from showProfile() so other functions can change data while modal is open
			i is the index of the profile in vm.users
		*/
		function setModalInfo(i) {

			//get the profile information
			//check if the info is 'truthy' (ie. not null, empty, 0, etc.)

			vm.picture = null;      /* <--------------- No longer needed? */

			if (vm.users[i].name) {
				vm.name = vm.users[i].name;
				vm.header = vm.users[i].name + "'s Profile";
			} else {
				vm.name = "Name Unknown";
				vm.header = "Unknown Profile";
			}

			if (vm.users[i].position) {
				vm.position = vm.users[i].position;
			} else {
				vm.position = "Position Unknown";
			}

			if (vm.users[i].department) {
				if (vm.users[i].department.dname != null) {
					vm.department = vm.users[i].department.dname;
					if (vm.users[i].department.office.id != null) {
						vm.office = vm.users[i].department.office.name;
					} else {
						vm.office = "Office Unknown";
					}
				} else {
					vm.department = "Department Unknown";
				}
			} else {
				vm.department = "Department Unknown";
			}



			if (vm.users[i].staffStatus != null) {
				vm.staffStatus = vm.users[i].staffStatus;
			}

			if (vm.users[i].phone) {
				vm.phone = vm.users[i].phone;
			} else {
				vm.phone = "Phone No. Unknown";
			}

			if (vm.users[i].email) {
				vm.email = vm.users[i].email;
			} else {
				vm.email = "Email Unknown";
			}

			if (vm.users[i].staffNum) {
				vm.staffNum = vm.users[i].staffNum;
			} else {
				vm.staffNum = "Staff No. Unknown";
			}


			if (vm.users[i].language) {
				vm.language = vm.users[i].language;
			} else {
				vm.language = "Language Unknown";
			}

			if (vm.users[i].position) {
				vm.position = vm.users[i].position;
			} else {
				vm.position = "Position Unknown";
			}


			if (vm.staffStatus == true) {
				vm.isEnabled = true;
			} else {
				vm.isEnabled = false;
			}

		}

        /*
			Loads a Modal (popup-like thing) with a profile in it
			i is the index
		*/
        function showProfile(i) {

			//assign the profile array index, for profile editing functions
			//eg. setPosition()
			vm.modalIndex = i;


			//load the profile info into the modal
			setModalInfo(i);


			//reset the position
			vm.currentPosition = vm.positionList[0];

			//show the modal
			$('#profileModal').modal();
        }


		/*
			Changes a profile
			This function is deprecated
		*/
		function changeProfile(obj) {
        	obj.staffStatus = "Retired";
        	var toSent = {"Profile": obj, "office_id": obj.office.id, "department_id": obj.department.id};
        	return dataservice.changeProfile(toSent).then(function(data) {
				console.log(data);

        	});
        }

		/*
			Changes a profile's position to a given string
		*/
		function setPosition(i) {

        	var toSend = {"staffNum": vm.users[i].staffNum, "position": vm.currentPosition};
        	console.log("Sending: " + toSend.staffNum +", " + toSend.position);
            console.log('controller');
			return dataservice.setPosition(toSend).then(function(data) {
        		console.log(data);

				//if data is null then server has nothing to return
				if (data == false) {
					logger.error("[Database Error] profile not found");
				//if data is "fail" then there was a server error
				} else if (data == "fail") {
					logger.error("[Database Error] failed to modify profile position");
				//otherwise the data should be ok
				} else {
					vm.users[i].position = vm.currentPosition;
					setModalInfo(i);
				}

        	});
        }

		/*
			Sets a profile's status to Enabled
			i is the profile's index in vm.users
		*/
		function enableProfile(i) {
        	var toSend = vm.users[i].staffNum;
			console.log("to send");
			console.log(toSend);

			return dataservice.enableProfile(toSend).then(function(data) {
				console.log("data:")
        		console.log(data);

				//if data is null then server has nothing to return
				if (data == null) {
					logger.error("[Database Error] profile not found");
				//if data is "fail" then there was a server error
				} else if (data == "fail") {
					logger.error("[Database Error] failed to enable profile");
				//otherwise the data should be ok
				} else {
					vm.users[i].staffStatus = true;
					setModalInfo(i);
					logger.success("Enabled " + vm.users[i].name + "'s profile");
				}

        	});

        }


		/*
			Sets a profile's status to Disabled
			i is the profile's index in vm.users
		*/
		function disableProfile(i) {
        	var toSend = vm.users[i].staffNum;

			return dataservice.disableProfile(toSend).then(function(data) {
        		console.log(data);

				//if data is null then server has nothing to return
				if (data == null) {
					logger.error("[Database Error] profile not found");
				//if data is "fail" then there was a server error
				} else if (data == "fail") {
					logger.error("[Database Error] failed to disable profile");
				//otherwise the data should be ok
				} else {
					vm.users[i].staffStatus = false;
					setModalInfo(i);
					logger.success("Disabled " + vm.users[i].name + "'s profile");
				}

        	});

        }

		/*
			Transitions to a given profile page.
		*/
		function transitionToProfile(i)
		{
			$('#profileModal').modal("hide");
			$state.transitionTo("profile", {id: i});
		}

    }
})();
