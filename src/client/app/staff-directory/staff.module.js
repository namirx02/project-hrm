(function() {
    'use strict';

    angular.module('app.staff', [
        'app.core',
        'app.widgets'
      ]);
})();
