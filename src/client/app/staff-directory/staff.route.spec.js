/* jshint -W117, -W030 */
describe('staff routes', function () {
    describe('state', function () {
        var view = 'app/staff/staff.html';

        beforeEach(function() {
            module('app.staff', bard.fakeToastr);
            bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
        });

        beforeEach(function() {
            $templateCache.put(view, '');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should map state staff to url / ', function() {
            expect($state.href('staff', {})).to.equal('/');
        });

        it('should map /staff route to staff View template', function () {
            expect($state.get('staff').templateUrl).to.equal(view);
        });

        it('of staff should work with $state.go', function () {
            $state.go('staff');
            $rootScope.$apply();
            expect($state.is('staff'));
        });
    });
});
