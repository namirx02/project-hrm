(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('applicationservice', applicationservice);

    applicationservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function applicationservice($http, $q, exception, logger, config) {
        var service = {
            getPendingApplications: getPendingApplications,
            getReviewedApplications: getReviewedApplications,
            managePendings: managePendings,
            applyLeave: applyLeave,
            applyOvertime: applyOvertime,
            applyExpense: applyExpense
        };

        return service;

        function getPendingApplications(page, sortBy, query, types) {
            var url = config.host + '/pending-applications';

            var dtypes = [];
            if (types.length == 1) {
                dtypes.push(types[0].text.replace("-application", "Submission"));
            } else if (types.length == 2) {
                dtypes.push(types[0].text.replace("-application", "Submission"));
                dtypes.push(types[1].text.replace("-application", "Submission"));
            }

            query.dtypes = dtypes;

            return $http.post(url + '?page=' + page + '&sortBy=' + sortBy, query)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function getReviewedApplications(page, sortBy, query, types) {
            var url = config.host + '/reviewed-applications';
            var dtypes = [];
            if (types.length == 1) {
                dtypes.push(types[0].text.replace("-application", "Submission"));
            } else if (types.length == 2) {
                dtypes.push(types[0].text.replace("-application", "Submission"));
                dtypes.push(types[1].text.replace("-application", "Submission"));
            }

            query.dtypes = dtypes;

            return $http.post(url + '?page=' + page + '&sortBy=' + sortBy, query)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function managePendings(obj) {
            return $http.post(config.host + '/manage-pendings', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function applyLeave(obj) {
        	return $http.post(config.host + '/leaves', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
            	return false;
                //return exception.catcher('XHR Failed for signup')(e);
            }
        }

        function applyOvertime(obj) {
            return $http.post(config.host + '/overtimes', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function applyExpense(obj) {
            return $http.post(config.host + '/expenses', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }
    }
})();
