(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('adminservice', adminservice);

    adminservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function adminservice($http, $q, exception, logger, config) {
        var service = {
            createProfile: createProfile
        };

        return service;

        function createProfile(obj) {
        	return $http.post(config.host + '/profiles', obj, {headers: {'Content-Type': 'multipart/form-data'}})
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
            	logger.error("There is an existing account with this email!");
            	return;
                //return exception.catcher('XHR Failed for signup')(e);
            }
        }
    }
})();
