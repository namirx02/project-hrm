(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[Huijin Error] ',
        appTitle: 'Huijin Xinda Ltd.',
        host: 'http://localhost:8000/api'
//      host: 'http://10.12.162.31:8000/api'
//    	host: 'http://203.10.91.71:8000/api'
    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$logProvider', 'routerHelperProvider', 'exceptionHandlerProvider', '$httpProvider', '$translateProvider'];
    /* @ngInject */
    function configure($logProvider, routerHelperProvider, exceptionHandlerProvider, $httpProvider, $translateProvider) {
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({docTitle: config.appTitle + ': '});
        $httpProvider.defaults.withCredentials = true;

        $translateProvider
            .preferredLanguage('en')
            .useStaticFilesLoader({
                prefix: '/app/i18n/',
                suffix: '.json'
            })
            .useSanitizeValueStrategy('sanitize');
    }

})();
