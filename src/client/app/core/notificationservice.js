(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('notificationservice', notificationservice);

    notificationservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function notificationservice($http, $q, exception, logger, config) {
        var service = {
            getNotification: getNotification,
            setViewed: setViewed
        };

        return service;

        function getNotification(){
            return $http.get(config.host + '/user-notification')
                  .then(success)
                  .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e){
                return;
            }
        }

        function setViewed(id){
            return $http.post(config.host + '/view-notification?notifyID=' + id)
                  .then(success)
                  .catch(fail);

            function success(response){
                return true;
            }

            function fail(e){
                return;
            }
        }

    }
})();
