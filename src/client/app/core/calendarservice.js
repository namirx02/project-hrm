(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('calendarservice', calendarservice);

    calendarservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function calendarservice($http, $q, exception, logger, config) {
        var service = {
            addEvent: addEvent,
            deleteEvent: deleteEvent,
            getUserEvents: getUserEvents,
            formatDate: formatDate
        };

        return service;

        function getUserEvents(offset, from, to, owner) {

            return $http.get(config.host + '/events?offset=' + offset + '&from=' + from + '&to=' + to + '&owner=' + owner)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                logger.error("Events are not available!");
                return;
            }
        }

        function addEvent(obj) {
            return $http.post(config.host + '/events', obj)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                logger.error("Events are not available!");
                return;
            }
        }

        function deleteEvent(id) {
            return $http.delete(config.host + '/events/' + id)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                logger.error("Events are not available!");
                return;
            }
        }

        function formatDate(strDate, strFormat) {
            var date = new Date(strDate);
            var fdate = moment(date).format(strFormat);
            return fdate;
        }
    }
})();
