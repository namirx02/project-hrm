(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('profileservice', profileservice);

    profileservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function profileservice($http, $q, exception, logger, config) {
        var service = {
            getProfileInfo: getProfileInfo,
            updateProfile: updateProfile,
            getAllProfiles: getAllProfiles,
            getProfilesByString: getProfilesByString,
            uploadContract: uploadContract
        };

        return service;

        function getAllProfiles(query) {
            return $http.post(config.host + '/profiles/query', query)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function getProfileInfo(id) {
        	return $http.get(config.host + '/profiles/' + id)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
            	logger.error("No employee with this ID!");
            	return;
            }
        }

        function updateProfile(obj) {
            console.log(obj);
            return $http.put(config.host + '/profiles/' + obj.Profile.staffNum, obj)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                logger.error("No employee with this ID!");
                return;
            }
        }

        function getProfilesByString(obj) {
            console.log(obj);
            return $http.get(config.host + '/profiles/string?page=' + obj.page + '&sortBy=' + obj.string)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function uploadContract(obj) {
            return $http.post(config.host + '/profiles/contract', obj, {headers: {'Content-Type': 'multipart/form-data'}})
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
                //return exception.catcher('XHR Failed for signup')(e);
            }
        }
    }
})();
