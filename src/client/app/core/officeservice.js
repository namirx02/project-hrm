(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('officeservice', officeservice);

    officeservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function officeservice($http, $q, exception, logger, config) {
        var service = {
            getOffices: getOffices,
            getDepartments: getDepartments,
            createOffice: createOffice,
            createDepartment: createDepartment
        };

        return service;

        function getOffices() {
            return $http.get(config.host + '/offices?page=1&sortBy=')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

        function getDepartments() {
            return $http.get(config.host + '/departments?page=1&sortBy=')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

        function createOffice(obj) {

            return $http.post(config.host + '/offices', obj)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }

        }


        function createDepartment(obj) {

            return $http.post(config.host + '/departments', obj)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }

        }
    }
})();
