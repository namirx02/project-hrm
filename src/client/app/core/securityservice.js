(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('securityservice', securityservice);

    securityservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function securityservice($http, $q, exception, logger, config) {
        var service = {
            login: login,
            logout: logout,
            changePassword: changePassword
        };

        return service;


        function login(email, password) {
            return $http.post(config.host + '/login?email=' + email + '&password=' + password)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function logout(id) {
            return $http.post(config.host + '/logout?id=' + id, {}, {withCredentials: true})
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function changePassword(obj) {
            return $http.post(config.host + '/password-change', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }
    }
})();
