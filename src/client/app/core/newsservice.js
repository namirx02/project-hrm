(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('newsservice', newsservice);

    newsservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function newsservice($http, $q, exception, logger, config) {
        var service = {
            postNews: postNews,
            getPublished: getPublished,
            getDraft: getDraft,
            deleteNews: deleteNews,
            getNews: getNews,
            updateNews: updateNews
        };

        return service;

        function postNews(obj) {
            return $http.post(config.host + '/news', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function updateNews(obj) {
            return $http.put(config.host + '/news', obj)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function deleteNews(id) {
            return $http.delete(config.host + '/news/' + id)
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function getPublished() {
            return $http.get(config.host + '/news/published')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function getNews(offset, length, sortBy) {
            return $http.get(config.host + '/news?offset=' + offset + '&length=' + length + '&sortBy=' + sortBy)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function getDraft() {
            return $http.get(config.host + '/news/draft')
                .then(success)
                .catch(fail);

            function success(response, status, header, config) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }
    }
})();
