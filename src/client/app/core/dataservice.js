(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function dataservice($http, $q, exception, logger, config) {
        var service = {
            getProfiles: getProfiles,
			countProfiles: countProfiles,

			getProfilesByLetter: getProfilesByLetter,
			getProfilesByOffice: getProfilesByOffice,
			getProfilesByDepartment: getProfilesByDepartment,
			getProfilesByString: getProfilesByString,
			countProfilesByLetter: countProfilesByLetter,
			countProfilesByOffice: countProfilesByOffice,
			countProfilesByDepartment: countProfilesByDepartment,
			countProfilesByString: countProfilesByString,

            getOffices: getOffices,
            getDepartments: getDepartments,
			getDepartmentsByOffice: getDepartmentsByOffice,

            setPrivilege: setPrivilege,
            countPages: countPages,
            enableProfile: enableProfile,
            disableProfile: disableProfile,
            setPosition: setPosition,
            resizePage:resizePage,   // fixes the footer location and huge white space

			test:test
        };

        return service;

        function resizePage(){
            setTimeout(function() {
                $.AdminLTE.layout.fix();
                $.AdminLTE.layout.fixSidebar();

            },10);

        }

        function setPosition(obj) {
            console.log(obj);
            return $http.post(config.host + '/position/' + obj.staffNum + '/' + obj.position)
                .then(success)
                .catch(fail);

            function success(response) {
                console.log(response);
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

        function getProfiles(i) {
            return $http.get(config.host + '/profiles?page=' + i + '&sortBy=')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function countProfiles() {
            return $http.get(config.host + '/profiles/count/all')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function getProfilesByLetter(obj) {
			console.log(obj);
            return $http.get(config.host + '/profiles/letter?page=' + obj.page + '&sortBy=' + obj.letter)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function countProfilesByLetter(i) {
			console.log(i);
			console.log("counting profiles with the letter " + i);
            return $http.get(config.host + '/profiles/count/letter?sortBy=' + i )
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function getProfilesByOffice(obj) {
			console.log(obj);
            return $http.get(config.host + '/profiles/office?page=' + obj.page + '&sortBy=' + obj.office)
                .then(success)
                .catch(fail);

            function success(response) {
				console.log("response:");
				console.log(response.data);
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }
		
		function countProfilesByOffice(i) {
			console.log(i);
			console.log("counting profiles with the letter " + i);
            return $http.get(config.host + '/profiles/count/office?sortBy=' + i )
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function getProfilesByDepartment(obj) {
			console.log(obj);
            return $http.get(config.host + '/profiles/department?page=' + obj.page + '&sortBy=' + obj.department)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }
		
		function countProfilesByDepartment(i) {
			console.log(i);
			console.log("counting profiles with the letter " + i);
            return $http.get(config.host + '/profiles/count/department?sortBy=' + i )
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function getProfilesByString(obj) {
			console.log(obj);
            return $http.get(config.host + '/profiles/string?page=' + obj.page + '&sortBy=' + obj.string)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function countProfilesByString(i) {
			console.log(i);
			console.log("counting profiles with the string " + i);
            return $http.get(config.host + '/profiles/count/string?sortBy=' + i )
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }


        function getOffices() {
            return $http.get(config.host + '/offices?page=1&sortBy=')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

        function getDepartments() {
            return $http.get(config.host + '/departments?page=1&sortBy=')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

		function getDepartmentsByOffice(i) {
			alert("get departments by office");
            return $http.get(config.host + '/departments/office?id=' + i)
                .then(success)
                .catch(fail);

            function success(response) {
				console.log("Response:");
				console.log(response.data);
                return response.data;
            }

            function fail(e) {
				console.log("get departments failed!");
                return "fail";
            }
        }

        function setPrivilege(obj) {
            console.log(obj);
            return $http.post(config.host + '/privilege/:staffNum=' + obj.staffNum + '/:privilege=' + obj.privilege)
                .then(success)
                .catch(fail);

            function success(response) {
                console.log(response);
                return response.data;
            }

            function fail(e) {
                return "fail";
            }
        }

        /*
         Determines how many pages needed for profiles
         */
        function countPages() {
            return $http.get(config.host + '/count')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(err) {
                return "fail";
            }
        }

        function enableProfile(staffNum) {

            return $http.post(config.host + '/status/:staffNum=' + staffNum + '/:staffStatus=true')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }

        }


        function disableProfile(staffNum) {

            return $http.post(config.host + '/status/:staffNum=' + staffNum + '/:staffStatus=false')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return "fail";
            }

        }

		function test() {
			return $http.get(config.host + '/global-leave/insert')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return null;
            }
		}

    }
})();
