(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('helperservice', helperservice);

    helperservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function helperservice($http, $q, exception, logger, config) {
        var service = {
            convertStringToDate: convertStringToDate,
            formatDate: formatDate
        };

        return service;

        function convertStringToDate(str) {
            var fstr = str.substr(0, str.indexOf('+')) + ' GMT' + str.substr(str.indexOf('+'), str.length);
            return new Date(fstr.replace("T", " "));
        }

        function formatDate(strDate, strFormat) {
            var date = new Date(strDate);
            var fdate = moment(date).format(strFormat);
            return fdate;
        }
    }
})();
