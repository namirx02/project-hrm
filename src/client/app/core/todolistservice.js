(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('todolistservice', todolistservice);

    todolistservice.$inject = ['$http', '$q', 'exception', 'logger', 'config'];
    /* @ngInject */
    function todolistservice($http, $q, exception, logger, config) {
        var service = {
            addItem: addItem,
            updateItem: updateItem,
            deleteItem: deleteItem,
            getItems: getItems,
            markDone: markDone,
            remindlater:remindlater
        };

        return service;

        function getItems() {
            return $http.get(config.host + '/todo')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function addItem(obj) {
            return $http.post(config.host + '/todo', obj)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function updateItem(obj) {
            return $http.put(config.host + '/todo', obj)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return false;
            }
        }

        function markDone(id){
            return $http.put(config.host + '/todo/markdone/' + id)
                .then(success)
                .catch(fail);

            function success(response){
                return response.data;
            }

            function fail(e){
                return false;
            }
        }

        function remindlater(obj){
            return $http.put(config.host + '/todo/remindlater', obj)
                .then(success)
                .catch(fail);

            function success(response){
                return response.data;
            }

            function fail(e){
                return false;
            }
        }

        function deleteItem(id) {
            return $http.delete(config.host + '/todo/' + id)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(e) {
                return;
            }
        }
    }
})();
