	(function () {
    'use strict';

    angular
        .module('app.login', ['ngCookies'])
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$http', '$q', 'securityservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope', '$cookies'];
    /* @ngInject */
    function LoginController($http, $q, securityservice, dataservice, logger, $state, $filter, $window, $rootScope, $cookies) {
        var vm = this;

        vm.title = 'Login';
        vm.login = login;

        activate();

        function activate() {
            if (sessionStorage.getItem('user')) {
                $state.transitionTo('dashboard');
            } else {
                $cookies.remove('JSESSIONID');
            }

            var promises = [hideSideBar(),dataservice.resizePage()];
            return $q.all(promises).then(function() {
                logger.info('Activated Login View');
            });
        }

        function hideSideBar(){     //remove sidebar
            $('body').addClass('layout-top-nav').removeClass('skin-black sidebar-mini');
        }

        function login(email, password) {
            return securityservice.login(email, password).then(function(data) {
                if (data == false) {
                    logger.error("Unable to login!");
                    return;
                }

                vm.loginUser = data;
                vm.loggedIn = true;

                sessionStorage.setItem('user', JSON.stringify(vm.loginUser));
                $rootScope.$broadcast("SuccessLogin", {
                    "user": vm.loginUser
                });

                $('body').addClass('skin-black sidebar-mini').removeClass('layout-top-nav');
                $state.transitionTo('dashboard');
            });
        }
    }
})();
