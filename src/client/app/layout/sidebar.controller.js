(function() {
    'use strict';

    angular
        .module('app.layout')
        .filter('dateInMillis', function() { //inline date filters
            return function(dateString) {
                return Date.parse(dateString);
            };
        })
        .controller('SidebarController', SidebarController);


    SidebarController.$inject = ['$state', 'routerHelper', '$window', '$rootScope', 'logger', 'securityservice', 'dataservice', 'notificationservice', 'config', '$websocket', '$q', 'todolistservice', '$interval', 'Notification', 'calendarservice'];
    /* @ngInject */
    function SidebarController($state, routerHelper, $window, $rootScope, logger, securityservice, dataservice, notificationservice, config, $websocket, $q, todolistservice, $interval, Notification, calendarservice) {
        var vm = this;
        var states = routerHelper.getStates();
        var calendarIntervalID, todoIntervalID;

        // variables
        vm.todoListItem = {};
        vm.displayedNotify = {};
        vm.calendarEvents = {};
        vm.offset;
        vm.currentStaffNum = 5;
        //declare function
        vm.logout = logout;
        vm.isCurrent = isCurrent;
        vm.getChildrenNavRoutes = getChildrenNavRoutes;
        vm.viewProfile = viewProfile;
        vm.getTitle = getTitle;
        vm.currentUser = "";
        vm.imageURL = "";
        vm.checkTaskDue = checkTaskDue;
        vm.initialiseToDoListItems = initialiseToDoListItems;
        vm.initialiseCalendarListItems = initialiseCalendarListItems;

        $rootScope.$on("SuccessLogin", function(event, data) {
            vm.currentUser = data.user.name;
            vm.imageURL = data.user.imageURL ? config.host + "/" + data.user.imageURL : "http://i.imgur.com/G8rcrgj.jpg";
            vm.isLoggedIn = true;
            sessionStorage.setItem("currentName", data.user.name);
            sessionStorage.setItem("imageURL", vm.imageURL);
          	getNavRoutes();
            initialiseToDoListItems();
            initialiseCalendarListItems();
        });

        $rootScope.$on("SuccessLogout", function() {
            vm.isLoggedIn = false;
        });

        $rootScope.$on("UpdateNotification", function(event, args) {
            vm.notification = args.unreadnotification;
            vm.allNotification = args.allNotification;
        });

        $rootScope.$on("NewCalendarEvent", function(event,args){
            // console.log("Calendar interval cleared: " + calendarIntervalID);
            // clearInterval(calendarIntervalID);
            initialiseCalendarListItems();
        });

        $rootScope.$on("NewTodoTask", function(event,args){
            initialiseToDoListItems();
        });

        activate();

        function activate() {
            var promises = [getNavRoutes(),init()];
            return $q.all(promises).then(function() {
                console.log("Called sidebar controller");
                repeatEvery(checkEventsAlert, 62000, 'calendar');  //calls check task due func every one minute
                repeatEvery(checkTaskDue, 62000, 'todolist');  //calls check task due func every one minute
            });
        }

        function init() {

            vm.offset = (new Date().getTimezoneOffset()/60)*(-1);

            if (sessionStorage.getItem("currentName")) {
                vm.currentUser = sessionStorage.getItem('currentName');
                vm.isLoggedIn = true;
                initialiseToDoListItems();
                initialiseCalendarListItems();
            }

            if (sessionStorage.getItem("imageURL")){
                vm.imageURL = sessionStorage.getItem('imageURL');
            }

        }


        // -----------------Func for calendar list reminder -----------------

        function initialiseCalendarListItems(){

            var tmpStartDate = new Date();
            var startDate = calendarservice.formatDate(tmpStartDate, 'YYYY-MM-DDTHH:mm:00');

            var tmpEndDate = addDaysToDate(new Date(), 2);  //2 days in future from today
            var endDate = calendarservice.formatDate(tmpEndDate, 'YYYY-MM-DDTHH:mm:00');

            return calendarservice.getUserEvents(vm.offset, startDate, endDate, vm.currentStaffNum).then(function(data){
                vm.calendarEvents = data;
                checkEventsAlert();
            });

        }

        function checkEventsAlert(){

            if(Object.keys(vm.calendarEvents).length > 0){
                console.log("Check events alert called");
                vm.calendarEvents.forEach(function (obj){
                    var currentDate = moment().seconds(0).milliseconds(0);
                    var start = moment(new Date(obj.start));

                    start.add((vm.offset * 3600 * 1000), "ms");
                    var alertDate = start.subtract(obj.alert, "m");

                    console.log('Title: ' + obj.title + ' current date:' + currentDate.format() + ' alert date: ' + alertDate.format());

                    if(currentDate.format() == alertDate.format()){ //If true, it means task is due.
                         Notification({message: obj.title + " coming up next!", title: 'Event Reminder'});
                    }
                });
            }
        }

        function addDaysToDate(currDate , days){
            var tmp = new Date(currDate.getTime() + (days*24*60*60*1000));
            return tmp;
        }

        // ----------------- END calendar list reminder --------------------

        // -----------------Func for to do list reminder ------------------

        function initialiseToDoListItems(){   //initialise to do list variables

            return todolistservice.getItems().then(function(data){
                vm.todoListItem = data;
                console.log(vm.todoListItem);
                checkTaskDue();
            });

        }

        function checkTaskDue(){
            if(Object.keys(vm.todoListItem).length > 0){
                vm.todoListItem.forEach(function (obj,i){ // iterate through to do list items and compare date
                    if(obj.timeDue != undefined && obj.timeDue != null && obj.done == false){
                        // var targetDate = new Date(obj.timeDue);
                        // var currentDate = new Date();

                        var targetDate = moment(new Date(obj.timeDue));
                        var currentDate = moment();
                        targetDate.add((vm.offset * 3600 * 1000), "ms");

                        console.log('Target Data: ' + targetDate.format() + ' Current Date: ' + currentDate.format());
                        if(targetDate <= currentDate){ //If true, it means task is due.
                            if(vm.displayedNotify[obj.id] == null || vm.displayedNotify[obj.id] == false){
                                Notification({message:obj.title, title: (obj.id.toString() + ':' + i), startTop:40, templateUrl: 'todolist-reminder-template.html', delay:null, closeOnClick:false});
                                vm.displayedNotify[obj.id] = true;
                            }
                        }
                    }
                });
            }
        }

        $rootScope.markComplete = function(rawIDIndex){

            var popout = this;
            //--- extract info from string
            var tmp = String(rawIDIndex);
            var IDandIndex = tmp.split(':');
            var id = IDandIndex[0];
            var index = IDandIndex[1];

            vm.displayedNotify[id] = false;

            todolistservice.markDone(id).then(function (data){
              if(data == true){
                vm.todoListItem[index].done = true;
                $rootScope.$broadcast('InitTodo');
                popout.kill(); //after complete processing, close notification
              }
           });


        }

        $rootScope.delayDate = function(delayID, rawIDIndex){
            var popout = this;
            //--- extract info from string
            var tmp = String(rawIDIndex);
            var IDandIndex = tmp.split(':');
            var id = IDandIndex[0];
            var index = IDandIndex[1];

            vm.displayedNotify[id] = false; // set displayed to false

            //process the Date
            var objDate = moment(new Date(vm.todoListItem[index].timeDue));
            objDate = objDate.add((vm.offset * 3600 * 1000), "ms");

            if(delayID == 1){
                objDate.add(5,'m');
            }else if(delayID == 2){
                objDate.add(1, 'h');
            }else if(delayID == 3){
                objDate.add(1, 'd');
            }else if(delayID == 4){
                objDate = '';
            }

            var toBeSent = {};
            toBeSent.id = id;

            if(objDate != ''){
                toBeSent.newDue = objDate.format('YYYY-MM-DDTHH:mm:00+' +vm.offset);
            }else{
                toBeSent.newDue = '';
            }

            var tmp = objDate;
            if (tmp != ''){
                vm.todoListItem[index].timeDue = tmp.subtract((vm.offset * 3600 * 1000), "ms");
            }else{
                vm.todoListItem[index].timeDue = '';
            }
            todolistservice.remindlater(toBeSent).then(function (data){
                if(data == true){
                    popout.kill(); //after complete processing, close notification
                }
            });
        }

        function repeatEvery(func, interval, type) {
            // Check current time and calculate the delay until next interval

            var tmpID;
            var now = new Date(), delay = interval - now % interval;

            function start() {
                // Execute function now...
                func();
                // ... and every interval
                tmpID = setInterval(func, interval);

                if(type == 'calendar'){
                    calendarIntervalID = tmpID;
                }else if(type == 'todolist'){
                    todoIntervalID = tmpID;
                }
            }

            window.onbeforeunload = function(e){
                if(calendarIntervalID != null){
                    clearInterval(calendarIntervalID);
                }

                if(todoIntervalID != null){
                    clearInterval(todoIntervalID);
                }
            };

            // Delay execution until it's an even interval
            setTimeout(start, delay);
        }
        // ------------------- END func for to do list -----------------

        function getNavRoutes() {
            vm.navRoutes = states.filter(function(r) {
                return r.settings && r.settings.nav < 2;
            }).sort(function(r1, r2) {
                return r1.settings.nav - r2.settings.nav;
            });
        }

        function getChildrenNavRoutes(parent) {
            return states.filter(function(r) {
                return r.settings && r.settings.nav === 2 && r.settings.parent === parent;
            }).sort(function(r1, r2) {
                return r1.settings.nav - r2.settings.nav;
            });
        }

        function isCurrent(route) {
            if (!route.title || !$state.current || !$state.current.title) {
                return '';
            }

            var menuName = route.title;
            var message = $state.current.title.substr(0, menuName.length) === menuName ? 'active' : '';

            if (message == 'active' && !route.hasChildren) {
                var elements = document.getElementsByClassName('treeview-menu');
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i].id != route.settings.parent + '-subtree') {
                        $(elements[i]).slideUp('slow');
                    }
                }

                elements = document.getElementsByClassName('active');
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i].id == route.settings.parent || elements[i].id == route.title) {} else {
                        elements[i].classList.remove('active');
                    }
                }

                if (route.settings.parent) {
                    var parent = document.getElementById(route.settings.parent);
                    var element = document.getElementById(route.settings.parent + '-subtree');

                    if (parent)
                        parent.classList.add('active');
                    if (element) {
                        element.classList.add('menu-open');
                        element.setAttribute('style', 'display: block');
                    }
                }
            }

            return message;
        }

        function getTitle(route) {
            return route.title;
        }

        function logout() {
            var sessionObj = sessionStorage.getItem('user');
            if (sessionObj) {
                sessionObj = JSON.parse(sessionObj);
                return securityservice.logout(sessionObj.staffNum).then(function(data) {
                    if (data == false) {
                        logger.error("Unable to perform this action!");
                    } else {
                        $rootScope.$broadcast("SuccessLogout");
                    }
                    sessionStorage.clear();
                    $state.transitionTo('login');
                });
            }


        }

        function viewProfile() {
            var sessionObj = sessionStorage.getItem('user');
            if (sessionObj) {
                sessionObj = JSON.parse(sessionObj);
                console.log(sessionObj);
                $state.transitionTo('profile', {id: sessionObj.staffNum});
            }
        }

    }
})();
