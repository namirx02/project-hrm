(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('htTopNav', htTopNav);

    /* @ngInject */
    function htTopNav () {
        var directive = {
            bindToController: true,
            controller: TopNavController,
            controllerAs: 'vm',
            restrict: 'EA',
            scope: {
                'navline': '='
            },
            templateUrl: 'app/layout/ht-top-nav.html'
        };

        TopNavController.$inject = ['$translate', '$state', 'routerHelper', '$window', '$rootScope', 'securityservice', 'dataservice', 'notificationservice', 'config', '$q'];
        /* @ngInject */
        function TopNavController($translate, $state, routerHelper, $window, $rootScope, securityservice, dataservice, notificationservice, config, $q) {
            var vm = this;
            var states = routerHelper.getStates();
            // variables
            vm.notification = {};
            vm.allNotification = {};
            vm.language = 'en';

            //declare function
            vm.activateRightNotificationBar = activateRightNotificationBar;
            vm.closeRightNotificationBar = closeRightNotificationBar;
            vm.notificationTransition = notificationTransition;
            vm.toggleLanguage = toggleLanguage;


            $rootScope.$on("UpdateNotification", function(event, args) {
                vm.notification = args.unreadnotification;
                vm.allNotification = args.allNotification;
            });

            activate();

            function activate() {
                var promises = [init()];
                return $q.all(promises).then(function() {
                    console.log("Called ht-top-navController");
                });
            }

            function init() {
                if (localStorage.getItem('language')) {
                    vm.language = localStorage.getItem('language');
                    $translate.use(vm.language);
                    return;
                }

                vm.language = 'en';
                $translate.use(vm.language);
                return;
            }

            function toggleLanguage(langKey) {
                vm.language = langKey;
                localStorage.setItem('language', langKey);
                $translate.use(vm.language);
            }

            // ------------------- Func for notification side bar -----------------
            function activateRightNotificationBar() {
                document.getElementById('rightNotificationBar').className += ' control-sidebar-open';
            }

            function closeRightNotificationBar() {
                document.getElementById('rightNotificationBar').classList.remove("control-sidebar-open");
            }

            function notificationTransition(state, id) { //transition for notification click
                var promises = [notificationservice.setViewed(id)];
                return $q.all(promises).then(function() {
                    $state.go(state, {}, {reload: true});
                });
            }
            // ---------------- END func for right side bar ----------------
        }

        return directive;
    }
})();
