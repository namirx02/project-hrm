(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('ShellController', ShellController);

    ShellController.$inject = ['$rootScope', '$timeout', 'config', 'logger'];
    /* @ngInject */
    function ShellController($rootScope, $timeout, config, logger) {
        var vm = this;
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.isLoggedIn = false;

        $rootScope.showSplash = true;
        vm.navline = {
            title: config.appTitle,
            text: 'Huijin Xinda HR System',
            link: 'http://www.huijinxinda.com.cn',
            today: new Date()
        };

        activate();

        $rootScope.$on("SuccessLogin", function() {
            vm.isLoggedIn = true;
        });

        $rootScope.$on("SuccessLogout", function() {
            vm.isLoggedIn = false;
        });

        function activate() {
            logger.success(config.appTitle + ' loaded!', null);
            hideSplash();

            if (sessionStorage.getItem('user')) {
                vm.isLoggedIn = true;
            }
        }

        function hideSplash() {
            //Force a 1 second delay so we can see the splash.
            $timeout(function() {
                $rootScope.showSplash = false;
            }, 1000);
        }
    }
})();
