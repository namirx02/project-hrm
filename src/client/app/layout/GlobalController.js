(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('GlobalController', GlobalController);

    GlobalController.$inject = ['$q', '$state', '$rootScope', '$timeout', 'config', 'logger', 'dataservice', 'notificationservice', '$websocket', 'Notification'];
    /* @ngInject */
    function GlobalController($q, $state, $rootScope, $timeout, config, logger, dataservice, notificationservice, $websocket, Notification) {
        var vm = this;
        //for websocket config
        var options = {};
        options.reconnectIfNotNormalClose = true;

        // variables
        vm.notification = {};
        vm.allNotification = {};

        // functions
        vm.getNotification = getNotification;
        vm.populateRightNotificationBar = populateRightNotificationBar;

        activate();

        function webSocketConnection() {
            var notificationSocket = $websocket('ws://localhost:8000/pushnotification', options);

            notificationSocket.onOpen(function (message) {
                var sessionUser = JSON.parse(sessionStorage.getItem('user'));
                notificationSocket.send('staffNum=' + sessionUser.staffNum);
            });

            notificationSocket.onMessage(function (message) {
                if (message.data == "pullnotification") {
                    notificationSocket.send("response=pull notification command received!");
                    getNotification();
                    Notification({message:'You have new notifications!', startTop:40});
                }else if(message.data == "pong"){
                    console.log("Pong received");
                }
            });

            notificationSocket.onError(function (message) {
                console.log(message);
            });

            notificationSocket.onClose(function (message) {
                notificationSocket.close();
            });

            setInterval(function(){notificationSocket.send('ping=hello server')},180000); //send ping to server to keep websocket alive
        }

        function init() {
          //pull notiication
          if ($state.current.name != "login") {
              getNotification();
          }

          //page refresh event - reconnect back the websocket
          window.onload = function(e){
              if(sessionStorage.getItem('socketConnected')){
                  webSocketConnection();
              }
          };

          //push notification
          if(sessionStorage.getItem('user') && !sessionStorage.getItem('socketConnected')){
              webSocketConnection();
              sessionStorage.setItem('socketConnected', true);
          }
        }

        function activate() {

            var promises = [dataservice.resizePage(),init()];
            return $q.all(promises).then(function() {
            });
        }

        function populateRightNotificationBar() {
            return notificationservice.getNotification().then(function(data) { //id is static for now (Test purpose)
                if (!data) {
                    vm.allNotification = {};
                } else {
                    vm.allNotification.list = data;
                }
            });
        }

        function getNotification() {
            return notificationservice.getNotification().then(function(data) { //id is static for now (Test purpose)
                if (!data) {
                    vm.notification = {};
                } else {
                    vm.allNotification = data;
                    vm.notification.unreadNotifications = Object(data.filter(function(n) {
                        return n.viewed === false;
                    }));
                    vm.notification.unreadCount = vm.notification.unreadNotifications.length;
                    $rootScope.$broadcast("UpdateNotification", {unreadnotification:vm.notification, allNotification:vm.allNotification});
                }
            });
        }
    }
})();
