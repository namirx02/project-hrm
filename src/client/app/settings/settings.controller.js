	(function () {
    'use strict';

    angular
        .module('app.settings')
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = ['$q', 'securityservice', 'dataservice', 'logger', '$state', '$filter', '$window', '$rootScope'];
    /* @ngInject */
    function SettingsController($q, securityservice, dataservice, logger, $state, $filter, $window, $rootScope) {
        var vm = this;

        vm.title = 'Settings';
        vm.change = {"newPassword": "", "oldPassword": ""};
        vm.newPasswordAgain = "";

        vm.changePassword = changePassword;

        activate();

        function activate() {
            var promises = [dataservice.resizePage(), init()];
            return $q.all(promises).then(function() {
                logger.info('Activated Settings View');
            });
        }

        function init() {
            vm.change = {"newPassword": "", "oldPassword": ""};
            vm.newPasswordAgain = "";
        }

        function changePassword(change, newPwdAgain) {
            switch (verifyPassword(change, newPwdAgain)) {
                case 0:
                    logger.error("Empty field!");
                    return;
                case -1:
                    logger.error("New password must be different to current password!");
                    return;
                case -2:
                    logger.error("New password not matches!");
                    return;
                default:
                    return securityservice.changePassword(vm.change).then(function(data) {
                       if (data == false)
                           logger.error("Unable to change password");
                       else
                           logger.success(data);

                       init();
                    });
            }
        }

        function verifyPassword(change, newPwdAgain) {
            if (!change.oldPassword || !change.newPassword || !newPwdAgain) {
                return 0;
            }

            if (change.oldPassword == change.newPassword) {
                return -1;
            }

            if (change.newPassword != newPwdAgain) {
                return -2;
            }

            return 1;
        }
    }
})();
