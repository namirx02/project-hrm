(function() {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'Settings',
                config: {
                    url: '/change-password',
                    templateUrl: 'app/settings/settings.html',
                    controller: 'SettingsController',
                    controllerAs: 'vm',
                    title: 'Settings',
                    settings: {
                        nav: 1,
                        // content: '<i class="settings-logo"></i> <span>Settings</span>'
                        name: "SETTINGS",
                        className: "settings-logo"
                    }
                }
            }
        ];
    }
})();
