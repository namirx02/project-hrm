# Sample HRM frontend

**Generated from HotTowel Angular**

>Template created by John Papa.

## Note

This is the WIP frontend repository that utilises Angular HotTowel Architecture and AdminLTE design components.

The repository integrates many common AngularJS 1 modules.

## Contributors

UOW Project group - S.K.Y.N.A TECH